#!/bin/bash

help_func(){

    echo ""
    echo "HELP INFORMATION"
    echo "    NAME:"
    echo "        RunProcessBackgroundSimulations.bash"
    echo "    PURPOSE:"
    echo "        This runs the ProcessBackgroundSimulations macro which runs over all the minimc files "
    echo "        for a particular energy, and event config and creates background distributions.."
    echo "    USAGE:"
    echo "        ./RunProcessBackgroundSimulations.bash [OPTION] ... [ENERGY] [EVENTCONFIG]"
    echo "    REQUIRED ARGUMENTS:"
    echo "        ENERGY - floating point number for the desired energy (7.7,11.5,14.5,19.6,27.0,39.0,62.4)"
    echo "        EVENTCONFIG - event configuration (ColliderCenter, ColliderPosY, ColliderNegY) "
    echo "    OPTIONS:"
    echo "        -h - display this help information"
    echo ""
    echo ""

}

#Check for options
while [ "$#" -gt 0 ]; do
    while getopts "p:h" opts; do
        case "$opts" in
            h) help_func; exit 1;;
            ?) exit 1;;
            *) echo "For help use option: -h"; exit 1;;
        esac
        shift
        OPTIND=1
    done

    if [ "$#" -gt 0 ]; then
        POSITIONALPARAM=(${POSITIONALPARAM[@]} $1)
        shift
        OPTIND=1
    fi
done

#Make sure only two argument remains after all the options
if [ "${#POSITIONALPARAM[@]}" -ne 2 ]; then
    echo "ERROR: This script requires only two arguments. For usage information use option -h."
    echo "       If you have used options check your formating."
    exit 1
fi

ENERGY=${POSITIONALPARAM[0]}
CONFIG=${POSITIONALPARAM[1]}

#Make sure all the required Variables are set
if [ -z $ENERGY ]; then
    echo "ENERGY is not set"
    exit 1
fi

if [ -z $CONFIG ]; then
    echo "CONFIG is not set"
    exit 1
fi

ENERGYNAME=""
if [ "$ENERGY" == 7.7 ]; then
    ENERGYNAME=AuAu07
elif [ "$ENERGY" == 11.5 ]; then
    ENERGYNAME=AuAu11
elif [ "$ENERGY" == 14.5 ]; then
    ENERGYNAME=AuAu14
elif [ "$ENERGY" == 19.6 ]; then
    ENERGYNAME=AuAu19
elif [ "$ENERGY" == 27.0 ]; then
    ENERGYNAME=AuAu27
elif [ "$ENERGY" == 39.0 ]; then
    ENERGYNAME=AuAu39
elif [ "$ENERGY" == 62.4 ]; then
    ENERGYNAME=AuAu62
fi

#Figure out where we are running, set the script name, and run the jobs
hostname=$( hostname )
if [[ $hostname == *"pdsf"* ]]; then
    XMLSCRIPTNAME=RunProcessBackgroundSimulations_pdsf.xml
    star-submit-template -template $XMLSCRIPTNAME -entities energy=$ENERGY,eventConfig=$CONFIG,energyName=$ENERGYNAME
    exit 0
elif [[ $hostname == *"rcf"* ]]; then
    XMLSCRIPTNAME=RunProcessBackgroundSimulations.xml
    star-submit-template -template $XMLSCRIPTNAME -entities energy=$ENERGY,eventConfig=$CONFIG,energyName=$ENERGYNAME
    exit 0
else
    echo "Running Locally on $hostname"
fi

###################################################################################################
#If we are running on neither rcf or pdsf then assume we are running locally

#Get the file containing the list of input files. Break it into smaller filelists
#so that the analysis can be done in parallel. 
INFILE=$PWD/../filelists/$ENERGYNAME\_$CONFIG\_BackgroundSim_MiniMC.local.list
if [ ! -e $INFILE ]; then
    echo "ERROR: The input file $INFILE does not exist"
    exit 1
fi

TOTALLINES=$(cat $INFILE | wc -l)
NOUTFILES=$(cat /proc/cpuinfo | grep -c processor)
RAND=$(cat /dev/urandom | tr -dc 'A-Z0-9' | fold -w 12 | head -n 1)
split --number=l/$NOUTFILES --additional-suffix=.tmp $INFILE $PWD/../filelists/$RAND
FILEARRAY=($(ls $PWD/../filelists/$RAND*.tmp))

#Create an output directory if needed
OUTDIR=$PWD/../data/
if [ ! -d "$OUTDIR" ]; then
    mkdir -p $OUTDIR
fi

processID=()
outFiles=()

#For each file in the file array run the process
for i in ${FILEARRAY[@]}; do

    tempOutFile=$(basename $i .tmp)
    tempOutFile=$OUTDIR/$tempOutFile.root
    outFiles+=($tempOutFile)
    
    nice root -l -b -q ../macros/RunProcessBackgroundSimulations.C\(\"$i\",\"$tempOutFile\",$ENERGY\) > /dev/null 2>&1 &

    processID+=($!)
    
done

wait ${processID[@]}

hadd -f $OUTDIR/$ENERGYNAME\_$CONFIG\_Background.root ${outFiles[@]}

rm ${FILEARRAY[@]}

rm ${outFiles[@]}

exit 0

