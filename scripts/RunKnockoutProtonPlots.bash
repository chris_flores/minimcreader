#!/bin/bash

help_func(){

    echo ""
    echo "HELP INFORMATION"
    echo "    NAME:"
    echo "        RunKnockoutProtonPlots.bash"
    echo "    PURPOSE:"
    echo "        This runs the KnockoutProtonPlots macro which runs over all the minimc files "
    echo "        for a particular energy, particle, and event config based on the arguments."
    echo "    USAGE:"
    echo "        ./RunProcessEmbedding.bash [OPTION] ... [ENERGY] [EVENTCONFIG]"
    echo "    REQUIRED ARGUMENTS:"
    echo "        ENERGY - floating point number for the desired energy (7.7,11.5,14.5,19.6,27.0,39.0,62.4)"
    echo "        EVENTCONFIG - event configuration (ColliderCenter, ColliderPosY, ColliderNegY) "
    echo "    OPTIONS:"
    echo "        -h - display this help information"
    echo "        -p - only do this particle id (0=pion, 1=kaon, 2=proton)"
    echo ""
    echo ""

}

#Check for options
while [ "$#" -gt 0 ]; do
    while getopts "p:h" opts; do
        case "$opts" in
            p) PARTICLEINDEX="${OPTARG}"; shift;;
            h) help_func; exit 1;;
            ?) exit 1;;
            *) echo "For help use option: -h"; exit 1;;
        esac
        shift
        OPTIND=1
    done

    if [ "$#" -gt 0 ]; then
        POSITIONALPARAM=(${POSITIONALPARAM[@]} $1)
        shift
        OPTIND=1
    fi
done

#Make sure only two argument remains after all the options
if [ "${#POSITIONALPARAM[@]}" -ne 2 ]; then
    echo "ERROR: This script requires only two arguments. For usage information use option -h."
    echo "       If you have used options check your formating."
    exit 1
fi

ENERGY=${POSITIONALPARAM[0]}
CONFIG=${POSITIONALPARAM[1]}

#Make sure all the required Variables are set
if [ -z $ENERGY ]; then
    echo "ENERGY is not set"
    exit 1
fi

if [ -z $CONFIG ]; then
    echo "CONFIG is not set"
    exit 1
fi

ENERGYNAME=""
if [ "$ENERGY" == 7.7 ]; then
    ENERGYNAME=AuAu07
elif [ "$ENERGY" == 11.5 ]; then
    ENERGYNAME=AuAu11
elif [ "$ENERGY" == 14.5 ]; then
    ENERGYNAME=AuAu14
elif [ "$ENERGY" == 19.6 ]; then
    ENERGYNAME=AuAu19
elif [ "$ENERGY" == 27.0 ]; then
    ENERGYNAME=AuAu27
elif [ "$ENERGY" == 39.0 ]; then
    ENERGYNAME=AuAu39
elif [ "$ENERGY" == 62.4 ]; then
    ENERGYNAME=AuAu62
fi

#Figure out where we are running and set the script name
hostname=$( hostname )
XMLSCRIPTNAME=""
if [[ $hostname == *"pdsf"* ]]; then
    XMLSCRIPTNAME=RunKnockoutProtonPlots_pdsf.xml
else
    XMLSCRIPTNAME=RunKnockoutProtonPlots.xml
fi

#If PARTICLEINDEX IS NOT SET THEN RUN OVER ALL PARTICLE SPECIES,
#OTHERWISE ONLY DO THE REQUESTED SPECIES
if [ -z $PARTICLEINDEX ]; then
    star-submit-template -template $XMLSCRIPTNAME -entities energy=$ENERGY,eventConfig=$CONFIG,pidName=PionPlus,pidIndex=0,charge=1,energyName=$ENERGYNAME
    star-submit-template -template $XMLSCRIPTNAME -entities energy=$ENERGY,eventConfig=$CONFIG,pidName=PionMinus,pidIndex=0,charge=-1,energyName=$ENERGYNAME
    star-submit-template -template $XMLSCRIPTNAME -entities energy=$ENERGY,eventConfig=$CONFIG,pidName=KaonPlus,pidIndex=1,charge=1,energyName=$ENERGYNAME
    star-submit-template -template $XMLSCRIPTNAME -entities energy=$ENERGY,eventConfig=$CONFIG,pidName=KaonMinus,pidIndex=1,charge=-1,energyName=$ENERGYNAME
    star-submit-template -template $XMLSCRIPTNAME -entities energy=$ENERGY,eventConfig=$CONFIG,pidName=ProtonPlus,pidIndex=2,charge=1,energyName=$ENERGYNAME
    star-submit-template -template $XMLSCRIPTNAME -entities energy=$ENERGY,eventConfig=$CONFIG,pidName=ProtonMinus,pidIndex=2,charge=-1,energyName=$ENERGYNAME
else
    if [ "$PARTICLEINDEX" == 0 ]; then
	star-submit-template -template $XMLSCRIPTNAME -entities energy=$ENERGY,eventConfig=$CONFIG,pidName=PionPlus,pidIndex=0,charge=1,energyName=$ENERGYNAME
	star-submit-template -template $XMLSCRIPTNAME -entities energy=$ENERGY,eventConfig=$CONFIG,pidName=PionMinus,pidIndex=0,charge=-1,energyName=$ENERGYNAME
    elif [ "$PARTICLEINDEX" == 1 ]; then
	star-submit-template -template $XMLSCRIPTNAME -entities energy=$ENERGY,eventConfig=$CONFIG,pidName=KaonPlus,pidIndex=1,charge=1,energyName=$ENERGYNAME
	star-submit-template -template $XMLSCRIPTNAME -entities energy=$ENERGY,eventConfig=$CONFIG,pidName=KaonMinus,pidIndex=1,charge=-1,energyName=$ENERGYNAME
    elif [ "$PARTICLEINDEX" == 2 ]; then
	star-submit-template -template $XMLSCRIPTNAME -entities energy=$ENERGY,eventConfig=$CONFIG,pidName=ProtonPlus,pidIndex=2,charge=1,energyName=$ENERGYNAME
	star-submit-template -template $XMLSCRIPTNAME -entities energy=$ENERGY,eventConfig=$CONFIG,pidName=ProtonMinus,pidIndex=2,charge=-1,energyName=$ENERGYNAME
    fi
fi
