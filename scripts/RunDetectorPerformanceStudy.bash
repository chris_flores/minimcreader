help_func(){

    echo ""
    echo "HELP INFORMATION"
    echo "    NAME:"
    echo "        RunDetectorPerformanceStudy.bash"
    echo "    PURPOSE:"
    echo "        This runs the Run-By-Run Detector Performance Study which creates eta V. phi "
    echo "        histograms for each run ID to evaulate how the performance changes with runs."
    echo "    USAGE:"
    echo "        ./RunDetectorPerformanceStudy.bash [OPTION] ... [ENERGY] [EVENTCONFIG]"
    echo "    REQUIRED ARGUMENTS:"
    echo "        ENERGY - floating point number for the desired energy (7.7,11.5,14.5,19.6,27.0,39.0,62.4)"
    echo "        EVENTCONFIG - event configuration (ColliderCenter, ColliderPosY, ColliderNegY) "
    echo "    OPTIONS:"
    echo "        -h - display this help information"
    echo ""
    echo ""

}

#Check for options
while [ "$#" -gt 0 ]; do
    while getopts "h" opts; do
	case "$opts" in
	    h) help_func; exit 1;;
	    ?) exit 1;;
	    *) echo "For help use option: -h"; exit 1;;
	esac
	shift
	OPTIND=1
    done

    if [ "$#" -gt 0 ]; then
	POSITIONALPARAM=(${POSITIONALPARAM[@]} $1)
	shift
	OPTIND=1
    fi
done

#Make sure only two argument remains after all the options
if [ "${#POSITIONALPARAM[@]}" -ne 2 ]; then
    echo "ERROR: This script requires only two arguments. For usage information use option -h."
    echo "       If you have used options check your formating."
    exit 1
fi

ENERGY=${POSITIONALPARAM[0]}
CONFIG=${POSITIONALPARAM[1]}

#Make sure all the required Variables are set
if [ -z $ENERGY ]; then
    echo "ENERGY is not set"
    exit 1
fi

if [ -z $CONFIG ]; then
    echo "CONFIG is not set"
    exit 1
fi

ENERGYNAME=""
if [ "$ENERGY" == 7.7 ]; then
    ENERGYNAME=AuAu07
elif [ "$ENERGY" == 11.5 ]; then
    ENERGYNAME=AuAu11
elif [ "$ENERGY" == 14.5 ]; then
    ENERGYNAME=AuAu14
elif [ "$ENERGY" == 19.6 ]; then
    ENERGYNAME=AuAu19
elif [ "$ENERGY" == 27.0 ]; then
    ENERGYNAME=AuAu27
elif [ "$ENERGY" == 39.0 ]; then
    ENERGYNAME=AuAu39
elif [ "$ENERGY" == 62.4 ]; then
    ENERGYNAME=AuAu62
fi

#Figure out where we are running and set the script name
hostname=$( hostname )
XMLSCRIPTNAME=""
if [[ $hostname == *"pdsf"* ]]; then
    XMLSCRIPTNAME=RunDetectorPerformance_pdsf.xml
else
    XMLSCRIPTNAME=RunDetectorPerformance.xml
fi

star-submit-template -template $XMLSCRIPTNAME -entities energy=$ENERGY,eventConfig=$CONFIG,energyName=$ENERGYNAME
