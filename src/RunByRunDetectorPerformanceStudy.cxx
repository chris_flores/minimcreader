#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#include <TFile.h>
#include <TString.h>
#include <TMath.h>
#include <TMath.h>
#include <TSystem.h>
#include <TF1.h>
#include <TH2D.h>
#include <TChain.h>
#include <TNtupleD.h>

#include "globalDefinitions.h"
#include "utilityFunctions.h"
#include "MiniMcReader.h"
#include "CentralityDatabaseClass.h"
#include "BinningAndCuts.h"

//Defined Below
int FindRunNumberIndex(std::vector<long> *runNumbers, long runNumber);
Int_t GetSectorID(Double_t trackEta, Double_t trackPhi);

//MAIN____________________________________________________
void RunByRunDetectorPerformanceStudy(const char *MINIMCLIST, TString OUTPUTFILE, TString CENTDATABASEFILE,
				      Double_t ENERGY){

  //Open the List of miniMC Files       
  ifstream miniMCList (MINIMCLIST);
  if (!miniMCList.is_open()){
    cout <<"MiniMCList not Open!" <<endl;
    exit (EXIT_FAILURE);
  }
  
  //Read All the Lines of the MINIMCLIST file and add them to the chain
  string miniMCFile;
  TChain *fileChain = new TChain("StMiniMcTree");
  while (miniMCList.good()){
    getline(miniMCList, miniMCFile);
    fileChain->Add(miniMCFile.c_str());
  }

  //Close the MiniMCList File  
  miniMCList.close();

  //Create the MiniMC Reader
  MiniMcReader *minimcReader = new MiniMcReader((TTree *)fileChain);

  //Load the Centrality Database File
  CentralityDatabase *centralityDatabase = NULL;
  TFile *centralityFile = new TFile(CENTDATABASEFILE,"READ");
  TTree *centralityTree = NULL;
  if (centralityFile->IsZombie() == false){ 
    centralityTree = (TTree *)centralityFile->Get("CentralityTree");
    if (!centralityTree){
      cout <<"ERROR - ProcessEmbedding() Centrality Tree not found! EXITING!\n";
      exit (EXIT_FAILURE);
    }
    centralityTree->SetBranchAddress("Runs",&centralityDatabase);
  }
  
  //Build The Run Number Look Up Table
  std::vector<Long64_t> runNumberLookUpTable =
    BuildRunNumberLookUpTable(centralityTree,centralityDatabase);
  
  //Set Event Vertex Parameters
  centralityTree->GetEntry(0);
  const double minVz = centralityDatabase->GetMinVz();
  const double maxVz = centralityDatabase->GetMaxVz();
  
  //Create the output File
  TFile *outFile = new TFile(OUTPUTFILE,"RECREATE");
  
  std::vector<long> processedRunNumbers; //List of run numbers that have been processed
  TH1D *ptEmbedded, *ptWeighted;
  std::vector<TH1D *> sectorIDHistoEmb;
  std::vector<TH1D *> sectorIDHistoMatch;
  std::vector<TH2D *> matchedTrackEtaPhiHisto; //List of 2D Matched TrackEta Vs. Phi Histos
  std::vector<TH2D *> embTrackEtaPhiHisto; //List of 2D Embedded Track Histos

  TF1 *ptWeightFunc = new TF1("ptWeightFunc","[0]*TMath::Exp(x/[1])",0,5);
  ptWeightFunc->SetParameters(1,-.12);
  ptEmbedded = new TH1D("ptEmbedded","Embedded pT (Unweighted pT); pT",200,0,5);
  ptWeighted = new TH1D("ptWeighted","Exponentially Weighted pT); pT",200,0,5);

 /******** LOOP OVER THE ENTRIES OF THE CHAIN **********/
  Long64_t nEntries = fileChain->GetEntries();
  for (Long64_t iEntry=0; iEntry<nEntries; iEntry++){

    minimcReader->Reset();
    fileChain->GetEntry(iEntry);

    Int_t centralityBin = GetCentralityBin(centralityTree,centralityDatabase,&runNumberLookUpTable,
                                           minimcReader->GetRunNumber(),minimcReader->GetEventNumber());

    if (centralityBin < 0)
      continue;

    //Get the Run Number Index
    int runIDIndex = FindRunNumberIndex(&processedRunNumbers,minimcReader->GetRunNumber());

    if (runIDIndex < 0){
      processedRunNumbers.push_back(minimcReader->GetRunNumber());
      sectorIDHistoEmb.push_back(new TH1D(Form("tpcEmbSectorID_%ld",minimcReader->GetRunNumber()),
					  Form("TPC SectorID Embedded Tracks | RunID=%ld;TPC Sector ID",
					       minimcReader->GetRunNumber()),24,0.5,24.5));
      sectorIDHistoMatch.push_back(new TH1D(Form("tpcMatchSectorID_%ld",minimcReader->GetRunNumber()),
					    Form("TPC SectorID Matched Tracks | RunID=%ld;TPC Sector ID",
						 minimcReader->GetRunNumber()),24,0.5,24.5));
      
      matchedTrackEtaPhiHisto.push_back(new TH2D(Form("matchedTrackEtaPhiHisto_%ld",minimcReader->GetRunNumber()),
					     Form("Matched Track #eta Vs. #phi | RunID=%ld;#eta;#phi",
						  minimcReader->GetRunNumber()),2,-1,1,12,-TMath::Pi(),TMath::Pi()));
      embTrackEtaPhiHisto.push_back(new TH2D(Form("embeddedTrackEtaPhiHisto_%ld",minimcReader->GetRunNumber()),
					     Form("Embedded Track #eta Vs. #phi | RunID=%ld;#eta;#phi",
						  minimcReader->GetRunNumber()),2,-1,1,12,-TMath::Pi(),TMath::Pi()));
      runIDIndex = processedRunNumbers.size()-1;
    }
    
    //***Loop Over the Embedded Tracks****
    for (Int_t embTrackIndex=0; embTrackIndex<minimcReader->GetNEmbeddedTracks(); embTrackIndex++){
      
      //Make Sure the Track is an embedded track and that it has the right Geant PID
      if (minimcReader->GetEmbTrackParentGeantID(embTrackIndex) != 0)
        continue;

      //Get Track pT Weight
      Double_t weight = ptWeightFunc->Eval(minimcReader->GetEmbTrackPt(embTrackIndex));

      sectorIDHistoEmb.at(runIDIndex)->Fill(GetSectorID(minimcReader->GetEmbTrackEta(embTrackIndex),
							minimcReader->GetEmbTrackPhi(embTrackIndex)),weight);
      embTrackEtaPhiHisto.at(runIDIndex)->Fill(minimcReader->GetEmbTrackEta(embTrackIndex),
					       minimcReader->GetEmbTrackPhi(embTrackIndex),weight);

    }//End Loop Over Embedded Tracks
    
    //***Loop Over the Matched Tracks ****
    for (Int_t matchTrackIndex=0; matchTrackIndex<minimcReader->GetNMatchedTracks(); matchTrackIndex++){

      //Make Sure the Track is an embedded track
      if (minimcReader->GetMatchedTrackParentGeantID(matchTrackIndex) != 0)
        continue;
      
      if (!IsGoodTrack(minimcReader,matchTrackIndex,"Matched"))
        continue;

      //Get Track pT Weight
      Double_t weight = ptWeightFunc->Eval(minimcReader->GetMatchedTrackPtEmb(matchTrackIndex));

      sectorIDHistoMatch.at(runIDIndex)->Fill(GetSectorID(minimcReader->GetMatchedTrackEtaReco(matchTrackIndex),
							  minimcReader->GetMatchedTrackPhiReco(matchTrackIndex)),weight);
      matchedTrackEtaPhiHisto.at(runIDIndex)->Fill(minimcReader->GetMatchedTrackEtaReco(matchTrackIndex),
						   minimcReader->GetMatchedTrackPhiReco(matchTrackIndex),weight);
      
      ptEmbedded->Fill(minimcReader->GetMatchedTrackPtReco(matchTrackIndex));
      ptWeighted->Fill(minimcReader->GetMatchedTrackPtReco(matchTrackIndex),weight);

    }//End Loop Over Matched Tracks
    
  }//End Loop Over Chain Entries

  //Save
  outFile->cd();
  ptEmbedded->Write();
  ptWeighted->Write();
  for (unsigned int i=0; i<matchedTrackEtaPhiHisto.size(); i++){
    if (embTrackEtaPhiHisto.at(i)->GetEntries() > 0){
      matchedTrackEtaPhiHisto.at(i)->Write();
      embTrackEtaPhiHisto.at(i)->Write();
      sectorIDHistoEmb.at(i)->Write();
      sectorIDHistoMatch.at(i)->Write();
    }
    
  }
  outFile->Close();
  
}

//___________________________________________________________
int FindRunNumberIndex(std::vector<long> *runNumbers, long runNumber){

  //Return the index for this run number, if the run number is not found
  //then return -1

  for (unsigned int i=0; i<runNumbers->size(); i++){

    if (runNumbers->at(i) == runNumber)
      return i;
    
  }//End Loop Over Run Numbers

  return -1;
}

//_________________________________________________________________
Int_t GetSectorID(Double_t trackEta, Double_t trackPhi){

  //Each TPC Sector covers 0.16666*Pi or 30 degrees
  //Phi = 0 along the x Axis and points to the center
  //of sector 3 in if Eta>0 and to the center of sector 21 if Eta<0
  const int nSectors(24);
  const double sectorAngle((2.0*TMath::Pi())/((double)nSectors/2.0)); //Angles are in Radians
  const double halfSectorAngle = sectorAngle/2.0;

  //Sector Boundaries: Sector ID is 1+arrayIndex for PosEta and arrayIndex
  double sectorStartBoundary[nSectors];
  double sectorStopBoundary[nSectors];

  //**** Assign Sector Boundaries Positive Eta****
  //Sector 3 //Points Along the Positive X Axis
  sectorStartBoundary[2] =  0.0 - halfSectorAngle;                
  sectorStopBoundary[2]  = sectorStartBoundary[2]+sectorAngle;
  
  //Sector 2
  sectorStartBoundary[1] = sectorStopBoundary[2];
  sectorStopBoundary[1]  = sectorStartBoundary[1]+sectorAngle;
  
  //Sector 1
  sectorStartBoundary[0] = sectorStopBoundary[1];
  sectorStopBoundary[0]  = sectorStartBoundary[0]+sectorAngle;
  
  //Sector 12
  sectorStartBoundary[11] = sectorStopBoundary[0];
  sectorStopBoundary[11]  = sectorStartBoundary[11]+sectorAngle;
  
  //Sector 11
  sectorStartBoundary[10] = sectorStopBoundary[11];
  sectorStopBoundary[10]  = sectorStartBoundary[10]+sectorAngle;
  
  //Sector 10
  sectorStartBoundary[9] = sectorStopBoundary[10];
  sectorStopBoundary[9]  = sectorStartBoundary[9]+sectorAngle;
  
  //Sector 9 is a special exception

  //Sector 4
  sectorStartBoundary[3] = sectorStartBoundary[2];
  sectorStopBoundary[3]  = sectorStartBoundary[3]-sectorAngle;
  
  //Sector 5
  sectorStartBoundary[4] = sectorStopBoundary[3];
  sectorStopBoundary[4]  = sectorStartBoundary[4]-sectorAngle;
  
  //Sector 6
  sectorStartBoundary[5] = sectorStopBoundary[4];
  sectorStopBoundary[5]  = sectorStartBoundary[5]-sectorAngle;
  
  //Sector 7
  sectorStartBoundary[6] = sectorStopBoundary[5];
  sectorStopBoundary[6]  = sectorStartBoundary[6]-sectorAngle;
  
  //Sector 8
  sectorStartBoundary[7] = sectorStopBoundary[6];
  sectorStopBoundary[7]  = sectorStartBoundary[7]-sectorAngle;

  //**** Assign Sector Boundaries Negative Eta****
  //Sector 21 //Points Along the Positive X Axis
  sectorStartBoundary[20] =  0.0 - halfSectorAngle;
  sectorStopBoundary[20]  = sectorStartBoundary[2]+sectorAngle;

  //Sector 22
  sectorStartBoundary[21] = sectorStopBoundary[20];
  sectorStopBoundary[21]  = sectorStartBoundary[21]+sectorAngle;

  //Sector 23
  sectorStartBoundary[22] = sectorStopBoundary[21];
  sectorStopBoundary[22]  = sectorStartBoundary[22]+sectorAngle;

  //Sector 24
  sectorStartBoundary[23] = sectorStopBoundary[22];
  sectorStopBoundary[23]  = sectorStartBoundary[23]+sectorAngle;

  //Sector 13
  sectorStartBoundary[12] = sectorStopBoundary[23];
  sectorStopBoundary[12]  = sectorStartBoundary[12]+sectorAngle;

  //Sector 14
  sectorStartBoundary[13] = sectorStopBoundary[12];
  sectorStopBoundary[13]  = sectorStartBoundary[13]+sectorAngle;

  //Sector 15 is a special Exception

  //Sector 20
  sectorStartBoundary[19] = sectorStartBoundary[20];
  sectorStopBoundary[19]  = sectorStartBoundary[19]-sectorAngle;

  //Sector 19
  sectorStartBoundary[18] = sectorStopBoundary[19];
  sectorStopBoundary[18]  = sectorStartBoundary[18]-sectorAngle;

  //Sector 18
  sectorStartBoundary[17] = sectorStopBoundary[18];
  sectorStopBoundary[17]  = sectorStartBoundary[17]-sectorAngle;

  //Sector 17
  sectorStartBoundary[16] = sectorStopBoundary[17];
  sectorStopBoundary[16]  = sectorStartBoundary[16]-sectorAngle;

  //Sector 16
  sectorStartBoundary[15] = sectorStopBoundary[16];
  sectorStopBoundary[15]  = sectorStartBoundary[15]-sectorAngle;

  //________________________________________________________________
  //Return the sector index depending on positive or negative eta
  //________________________________________________________________
  
  //Positive Eta
  if (trackEta >= 0){

    //Sector 3 Exception
    if (fabs(trackPhi) < sectorStopBoundary[2])
      return 3;
    
    //Sector 9 Exception
    if (fabs(trackPhi) > sectorStopBoundary[9])
      return 9;
    
    for (int iSector=0; iSector<12; iSector++){

      //Skip Sectors 3 and 9
      if (iSector == 2 || iSector == 8)
	continue;

      if (trackPhi/fabs(trackPhi) > 0){
	if (trackPhi > sectorStartBoundary[iSector] && trackPhi <= sectorStopBoundary[iSector])
	  return iSector+1;
      }
      else {
	if (trackPhi <= sectorStartBoundary[iSector] && trackPhi > sectorStopBoundary[iSector])
	  return iSector+1;
      }
      
    }

    cout <<trackEta <<" " <<trackPhi <<" " <<trackPhi*(180.0/TMath::Pi())<<endl;
    
  }//End Positive Eta

  //Negative Eta
  if (trackEta < 0){

    //Sector 21 Exception
    if (fabs(trackPhi) < sectorStopBoundary[20])
      return 21;
    
    //Sector 15 Exception
    if (fabs(trackPhi) > sectorStopBoundary[13])
      return 15;
    
    for (int iSector=12; iSector<nSectors; iSector++){

      //Skip Sectors 21 and 15
      if (iSector == 14 || iSector == 20)
	continue;

      if (trackPhi/fabs(trackPhi) > 0){
	if (trackPhi > sectorStartBoundary[iSector] && trackPhi <= sectorStopBoundary[iSector])
	  return iSector+1;
      }
      else {
	if (trackPhi <= sectorStartBoundary[iSector] && trackPhi > sectorStopBoundary[iSector])
	  return iSector+1;
      }

    }

  }//End Negative Eta
  
  //Sector Not found
  return -1;
}

