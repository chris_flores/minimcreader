#include <iostream>
#include <vector>
#include <fstream>
#include <string>

#include <TFile.h>
#include <TString.h>
#include <TMath.h>
#include <TSystem.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TH3F.h>
#include <TChain.h>
#include <TNtupleD.h>

#include "globalDefinitions.h"
#include "utilityFunctions.h"
#include "MiniMcReader.h"
#include "CentralityDatabaseClass.h"
#include "BinningAndCuts.h"

/***************************************************
ON EVENT CUTS:
The centrality database contains the run number, event number, 
and centrality bin index for each event that has already passed the
event selection criteria for the analysis. Thus, if a centrality bin
is not found for an event it is a bad event and can be rejected. This means
we do not have to apply any event cuts in this code.

ON TRACK CUTS:
You should implement your tracks cuts in the IsGoodTrack() function which
is located in src/BinningAndCuts.cxx
*****************************************************/

//MAIN_____________________________________________
void ProcessEmbedding(const char *MINIMCLIST, TString OUTPUTFILE, TString CENTDATABASEFILE,
		      Double_t ENERGY, Int_t PIDINTEREST, Int_t CHARGE){

  const int nCentBins = 9;
  TString energy = TString::Format("#sqrt{s_{NN}} = %g GeV",ENERGY);
  const char *pidSymbol = GetParticleSymbol(PIDINTEREST,CHARGE).Data();

  //Open the List of miniMC Files       
  ifstream miniMCList (MINIMCLIST);
  if (!miniMCList.is_open()){
    cout <<"MiniMCList not Open!" <<endl;
    exit (EXIT_FAILURE);
  }
  
  //Read All the Lines of the MINIMCLIST file and add them to the chain
  string miniMCFile;
  TChain *fileChain = new TChain("StMiniMcTree");
  while (miniMCList.good()){
    getline(miniMCList, miniMCFile);
    fileChain->Add(miniMCFile.c_str());
  }

  //Close the MiniMCList File  
  miniMCList.close();

  //Create the MiniMC Reader
  MiniMcReader *minimcReader = new MiniMcReader((TTree *)fileChain);
  
  //Load the Centrality Database File
  CentralityDatabase *centralityDatabase = NULL;
  TFile *centralityFile = new TFile(CENTDATABASEFILE,"READ");
  TTree *centralityTree = NULL;
  if (centralityFile->IsZombie() == false){ 
    centralityTree = (TTree *)centralityFile->Get("CentralityTree");
    if (!centralityTree){
      cout <<"ERROR - ProcessEmbedding() Centrality Tree not found! EXITING!\n";
      exit (EXIT_FAILURE);
    }
    centralityTree->SetBranchAddress("Runs",&centralityDatabase);
  }

  //Build The Run Number Look Up Table
  std::vector<Long64_t> runNumberLookUpTable =
    BuildRunNumberLookUpTable(centralityTree,centralityDatabase);

  //Set Event Vertex Parameters
  centralityTree->GetEntry(0);
  const double minVz = centralityDatabase->GetMinVz();
  const double maxVz = centralityDatabase->GetMaxVz();

  //Create the output File
  TFile *outFile = new TFile(OUTPUTFILE,"RECREATE");
  
  //************ CREATE HISTOGRAMS ***********************//
  //NEvents Histograms
  TH1F *nEventsHisto = new TH1F("nEvents","Number of Events: 0=total, 1=used, 2=unused",3,0,3);
  
  //QA Histograms for Embedded Events Before Any Cuts
  TH1F *embZVertexHistoNoCuts;
  TH2F *embXYVertexHistoNoCuts;
  embZVertexHistoNoCuts = new TH1F("embZVertexHistoNoCuts",
				   Form("V_{z} %s %s (No Cuts);z Vertex (cm)",pidSymbol,energy.Data()),
				   100,minVz-10,maxVz+10);
  embXYVertexHistoNoCuts = new TH2F("embXYVertexHistoNoCuts",
				    Form("V_{x},V_{y} %s %s (No Cuts);x Vertex (cm);y Vertex (cm)",pidSymbol,energy.Data()),
				    100,-6,6,100,-6,6);
  
  //QA Histograms of Non-Usabe Events
  TH1F *embZVertexHistoBad;
  TH2F *embXYVertexHistoBad;
  TH1F *embCentralMultHistoBad,*embOriginMultHistoBad;
  TNtupleD *badEvents = new TNtupleD("badEvents","badEvents","x:y:z:runId:eventId:centralMult:originMult",100000);
  
  embZVertexHistoBad = new TH1F("embZVertexHistoBad",
				Form("V_{z} %s %s (Unusabe Emb Events);z Vertex (cm)",pidSymbol,energy.Data()),
				450,-225,225);
  embXYVertexHistoBad = new TH2F("embXYVertexHistoBad",
				 Form("V_{x},V_{y} %s %s (Unusable Emb Events);x Vertex (cm);y Vertex (cm)",
				      pidSymbol,energy.Data()),100,-6,6,100,-6,6);
  embCentralMultHistoBad = new TH1F("embCentralMultHistoBad",
				    Form("Central Multiplicity %s %s (Unusable Event); centralMult",
					 pidSymbol,energy.Data()),2000,0,2000);
  embOriginMultHistoBad = new TH1F("embOriginMultHistoBad",
				   Form("Origin Multiplicity %s %s (Unusable Event); centralMult",
					pidSymbol,energy.Data()),2000,0,2000);
  
  //QA Histograms for Embedded Event Quantities
  TH1F *embZVertexHisto, *embEventPhiHisto;
  TH1F *nEmbTrackHisto, *nMatchTrackHisto, *refMultHisto, *originMultHisto;
  TH1F *centralityHisto, *centralMultHisto;
  TH2F *embXYVertexHisto;
  
  nEmbTrackHisto   = new TH1F("nEmbTrackHisto",
			      Form("nEmbeddedTracks %s %s;nEmbTracks",pidSymbol,energy.Data()),
			      minimcReader->GetNMaxEntries(),0,minimcReader->GetNMaxEntries());
  nMatchTrackHisto = new TH1F("nMatchTrackHisto",
			      Form("nMatchedTracks %s %s;nMatchTracks",pidSymbol,energy.Data()),
			      20,0,20);
  refMultHisto     = new TH1F("refMultHisto",
			      Form("RefMult %s %s;refMult",pidSymbol,energy.Data()),
			      400,0,400);
  originMultHisto  = new TH1F("originMultHisto",
			      Form("N Daughters %s %s;originMult (number of daughters)",pidSymbol,energy.Data()),
			      400,0,400);
  centralityHisto  = new TH1F("centralityHisto",Form("Centrality %s %s;centrality",pidSymbol,energy.Data()),
			      400,0,400);
  centralMultHisto = new TH1F("centralMultHisto",Form("Multiplicity %s %s;centralMult",pidSymbol,energy.Data()),
			      400,0,400);
  embZVertexHisto  = new TH1F("embZVertexHisto",
			      Form("V_{z} %s %s;z Vertex (cm)",pidSymbol,energy.Data()),
			      100,minVz-10,maxVz+10);
  embEventPhiHisto = new TH1F("embEventPhiHisto",
			      Form("Event #phi %s %s;Event #phi",pidSymbol,energy.Data()),
			      100,-3.2,3.2);
  embXYVertexHisto = new TH2F("embXYVertexHisto",
			      Form("V_{x},V_{y} %s %s;x Vertex (cm);y Vertex (cm)",pidSymbol,energy.Data()),
			      100,-6,6,100,-6,6);
  
  //QA Histograms for Embedded Track Quantities
  TH2F *embTrackEtaPhiHisto, *embTrackRapidityPtHisto;
  TH1F *embTrackEtaHisto, *embTrackPhiHisto, *embTrackRapidityHisto, *embTrackPtHisto, *embTrackmTm0Histo;
  TH3F *embTrackRapidityPtPhiHisto, *embTrackRapidityPtNHitsHisto, *embTrackEtaPtPhiHisto;
  
  embTrackEtaPhiHisto     = new TH2F("embTrackEtaPhiHisto",
				     Form("EmbTrack #eta Vs. #phi %s %s;#eta;#phi",pidSymbol,energy.Data()),
				     100,-2,2,100,-4,4);
  embTrackRapidityPtHisto = new TH2F("embTrackRapidityPtHisto",
				     Form("EmbTrackRapidity Vs p_{T} %s %s;y_{%s};p_{T} (GeV)",
					  pidSymbol,energy.Data(),pidSymbol),
				     100,-2,2,100,0,2.0);
  embTrackEtaHisto        = new TH1F("embTrackEtaHisto",
				     Form("#eta %s %s;#eta",pidSymbol,energy.Data()),
				     100,-2,2);
  embTrackPhiHisto        = new TH1F("embTrackPhiHisto",
				     Form("EmbTrack #phi %s %s;#phi",pidSymbol,energy.Data()),
				     100,-4.0,4.0);
  embTrackRapidityHisto   = new TH1F("embTrackRapidityHisto",
				     Form("EmbTrack Rapidity %s %s;y_{%s}",
					  pidSymbol,energy.Data(),pidSymbol),
				     100,-2,2);
  embTrackPtHisto         = new TH1F("embTrackPtHisto",
				     Form("EmbTrack p_{T} %s %s;p_{T} (GeV)",pidSymbol,energy.Data()),
				     100,0,2.0);
  embTrackmTm0Histo       = new TH1F("embTrackmTm0Histo",
				     Form("EmbTrack m_{T}-m_{%s} %s %s;m_{T}-m_{%s} (GeV)",
					  pidSymbol,pidSymbol,energy.Data(),pidSymbol),100,0,2.0);
  embTrackRapidityPtPhiHisto = new TH3F("embTrackRapidityPtPhiHisto",
					Form("EmbTrack m_{T}-m_{%s} %s %s;y_{%s};p_{T} (GeV);#phi",
					     pidSymbol,pidSymbol,energy.Data(),pidSymbol),
					nRapidityBins,rapidityMin,rapidityMax,10,0,2.0,60,-3.5,3.5);
  embTrackRapidityPtNHitsHisto = new TH3F("embTrackRapidityPtNHitsHisto",
					  Form("EmbTrack Rapidity Vs. p_{T} %s %s;y_{%s};p_{T} (GeV);NHits",
					       pidSymbol,energy.Data(),pidSymbol),
					  nRapidityBins,rapidityMin,rapidityMax,10,0,2.0,50,0,50);
  embTrackEtaPtPhiHisto        = new TH3F("embTrackEtaPtPhiHisto",
					  Form("EmbTrack #eta Vs. p_{T} %s %s;#eta;p_{T} (GeV);#phi",
                                               pidSymbol,energy.Data()),
                                          nRapidityBins,rapidityMin,rapidityMax,10,0,2.0,100,-4,4);
  
  //QA Histograms for the Matched Track Quantities                                                            
  TH2F *matchTrackEtaPhiHisto, *matchTrackRapidityPtHisto;
  TH1F *matchTrackEtaHisto, *matchTrackPhiHisto, *matchTrackRapidityHisto, *matchTrackPtHisto, *matchTrackmTm0Histo;
  TH3F *matchTrackRapidityPtPhiHisto, *matchTrackRapidityPtNHitsHisto;
  TH3F *matchTrackRapidityPtNHitsFitHisto, *matchTrackRapidityPtNHitsPossHisto, *matchTrackRapidityPtNHitsFracHisto;
  TH3F *matchTrackRapidityPtNHitsdEdxHisto, *matchTrackRapidityPtGlobalDCAHisto, *matchTrackEtaPtPhiHisto;
  TH3F *matchTrackEtaPembPrecoHisto, *matchTrackEtaPtembPtrecoHisto, *matchTrackEtaRapidityembRapidityrecoHisto;
  TH3F *matchTrackEtaPtdEdxHisto;
  
  matchTrackEtaPhiHisto     = new TH2F("matchTrackEtaPhiHisto",
				       Form("MatchedTrack #eta Vs. #phi %s %s;#eta;#phi",pidSymbol,energy.Data()),
				       100,-2,2,100,-4,4);
  matchTrackRapidityPtHisto = new TH2F("matchTrackRapidityPtHisto",
				       Form("MatchedTrack p_{T} %s %s;y_{%s};p_{T}",pidSymbol,energy.Data(),pidSymbol),
				       100,-2,2,100,0,2.0);
  matchTrackEtaHisto        = new TH1F("matchTrackEtaHisto",
				       Form("MatchedTrack #eta %s %s;#eta",pidSymbol,energy.Data()),
				       100,-2,2);
  matchTrackPhiHisto        = new TH1F("matchTrackPhiHisto",
				       Form("MatchedTrack #phi %s %s;#phi",pidSymbol,energy.Data()),
				       100,-4,4);
  matchTrackRapidityHisto   = new TH1F("matchTrackRapidityHisto",
				       Form("MatchedTrack Rapidity %s %s;y_{%s}",pidSymbol,energy.Data(),pidSymbol),
				       100,-2,2);
  matchTrackPtHisto         = new TH1F("matchTrackPtHisto",
				       Form("Matched Track p_{T} %s %s;p_{T} (GeV)",pidSymbol,energy.Data()),
				       100,0,2.0);
  matchTrackmTm0Histo       = new TH1F("matchTrackmTm0Histo",
				       Form("Matched Track m_{T}-m_{0} %s %s;m_{T}-m_{%s}",pidSymbol,energy.Data(),pidSymbol),
				       100,0,2.0);
  matchTrackRapidityPtPhiHisto = new TH3F("matchTrackRapidityPtPhiHisto",
					  Form("Matched Track Rapidity Vs. p_{T};y_{%s};p_{T} (GeV);#phi",
					       pidSymbol),
					  nRapidityBins,rapidityMin,rapidityMax,10,0,2.0,100,-4,4);
  matchTrackRapidityPtNHitsHisto = new TH3F("matchTrackRapidityPtNHitsHisto",
					    Form("Matched Track Rapidity Vs. p_{T} Vs. nHits;y_{%s};p_{T} (GeV);NHits",
						 pidSymbol),
					    nRapidityBins,rapidityMin,rapidityMax,10,0,2.0,50,0,50);
  matchTrackRapidityPtNHitsFitHisto = new TH3F("matchTrackRapidityPtNHitsFitHisto",
					       Form("Matched Track Rapidity Vs. p_{T} Vs nHitsFit;y_{%s};p_{T} (GeV);NHitsFit",
						    pidSymbol),
					       nRapidityBins,rapidityMin,rapidityMax,10,0,2.0,50,0,50);
  matchTrackRapidityPtNHitsPossHisto = new TH3F("matchTrackRapidityPtNHitsPossHisto",
						Form("Matched Track Rapidity Vs. p_{T} Vs. nHitsPoss;y_{%s};p_{T} (GeV);NHitsPoss",
						     pidSymbol),
						nRapidityBins,rapidityMin,rapidityMax,10,0,2.0,50,0,50);
  matchTrackRapidityPtNHitsFracHisto = new TH3F("matchTrackRapidityPtNHitsFracHisto",
						Form("Matched Track Rapidity Vs. p_{T} Vs. nHitsFrac;y_{%s};p_{T} (GeV);NHitsFrac",
						     pidSymbol),
						nRapidityBins,rapidityMin,rapidityMax,10,0,2.0,100,0,1);
  matchTrackRapidityPtNHitsdEdxHisto = new TH3F("matchTrackRapidityPtNHitsdEdxHisto",
						Form("Matched Track Rapidity Vs. p_{T} Vs. nHitsdEdx;y_{%s};p_{T} (GeV);NHitsdEdx",
                                                     pidSymbol),
                                                nRapidityBins,rapidityMin,rapidityMax,10,0,2.0,50,0,50);
  matchTrackRapidityPtGlobalDCAHisto = new TH3F("matchTrackRapidityPtGlobalDCAHisto",
						Form("Matched Track Rapidity Vs. p_{T} Vs. GlobalDCA (cm);y_{%s};p_{T} (GeV);GlobalDCA (cm)",
                                                     pidSymbol),
                                                nRapidityBins,rapidityMin,rapidityMax,10,0,2.0,50,0,5);
  matchTrackEtaPtPhiHisto            = new TH3F("matchTrackEtaPtPhiHisto",
					      "Matched Track #eta Vs. p_{T} Vs. #phi;#eta;p_{T} (GeV);#phi",
						nRapidityBins,rapidityMin,rapidityMax,10,0,2.0,100,-4,4);

  matchTrackEtaPembPrecoHisto        = new TH3F("matchTrackEtaPembPrecoHisto",
						"Matched Track #eta^{Emb} Vs. p^{Emb} Vs. p^{Reco};#eta^{Emb};|p^{Emb}| (GeV);|p^{Reco}| (GeV)",
						nRapidityBins,rapidityMin,rapidityMax,100,0,2.0,100,0,2.0);
  matchTrackEtaPtembPtrecoHisto        = new TH3F("matchTrackEtaPtembPtrecoHisto",
						  "Matched Track #eta^{Emb} Vs. p_{T}^{Emb} Vs. p_{T}^{Reco};#eta^{Emb};p_{T}^{Emb} (GeV);p_{T}^{Reco} (GeV)",                                                
						  nRapidityBins,rapidityMin,rapidityMax,100,0,2.0,100,0,2.0);
  matchTrackEtaRapidityembRapidityrecoHisto        = new TH3F("matchTrackEtaRapidityEmbRapidityRecoHisto",
							      Form("Matched Track #eta^{Emb} Vs. p_{Emb} Vs. p_{Reco};#eta^{Emb};y_{%s}^{Emb};y_{%s}^{Reco}",
								   pidSymbol,pidSymbol),
							      nRapidityBins,rapidityMin,rapidityMax,100,rapidityMin,rapidityMax,100,rapidityMin,rapidityMax);
  matchTrackEtaPtdEdxHisto = new TH3F("matchTrackEtaPtdEdxHisto",
				      "MatchTrack #eta^{Emb} Vs. p_{T}^{Emb} Vs. dEdx;#eta^{Emb};p_{T}^{Emb} (GeV);dE/dx (KeV/cm)",
				      nRapidityBins,rapidityMin,rapidityMax,10,0,2.0,100,0,10);

  //Split Track Histograms
  TH2F *splitTrackEtaPtHisto;

  splitTrackEtaPtHisto = new TH2F("splitTrackEtaPtHisto",
				  Form("Split Track #eta^{Emb} Vs. p_{T}^{Emb} %s;#eta^{Emb};p_{T} (GeV)",
				       pidSymbol),
				  nRapidityBins,rapidityMin,rapidityMax,10,0,2.0);

  //Efficiency Related Histograms
  std::vector< TH2F *> embTrackHisto (nCentBins,(TH2F *)NULL);
  std::vector< TH2F *> matchTrackHisto (nCentBins,(TH2F *)NULL);
  for (int centIndex=0; centIndex<nCentBins; centIndex++){
    embTrackHisto[centIndex] = new TH2F(Form("embTrackHisto_Cent%d",centIndex),
					Form("EmbTracks Rapidity Vs. m_{T}-m_{%s} %s;y_{%s}^{Emb};(m_{T}-m_{%s})^{Emb} (GeV)",
					     pidSymbol,pidSymbol,pidSymbol,pidSymbol),
					nRapidityBins,rapidityMin,rapidityMax,200,0,2.0);
    matchTrackHisto[centIndex] = new TH2F(Form("matchTrackHisto_Cent%d",centIndex),
					  Form("MatchedTracks Rapidity Vs. m_{T}-m_{%s} %s;y_{%s}^{Emb};(m_{T}-m_{%s})^{Emb} (GeV)",
					       pidSymbol,pidSymbol,pidSymbol,pidSymbol),
					  nRapidityBins,rapidityMin,rapidityMax,200,0,2.0);
  }

  //Energy Loss Related Histograms
  std::vector< TH3F *> pTLossHisto (nCentBins,(TH3F *)NULL);
  std::vector< TH3F *> rapidityLossHisto (nCentBins,(TH3F *)NULL);
  for (int centIndex=0; centIndex<nCentBins; centIndex++){
    pTLossHisto[centIndex] = new TH3F(Form("pTLossHisto_Cent%d",centIndex),
				      Form("p_{T} Loss, %s;y_{%s}^{Reco};p_{T}^{Reco} (GeV);p_{T}^{Reco}-p_{T}^{Emb} (GeV)",
					   pidSymbol,pidSymbol),
				      nRapidityBins,rapidityMin,rapidityMax,50,0,2.0,100,-.1,.1);
    rapidityLossHisto[centIndex] = new TH3F(Form("rapidityLossHisto_Cent%d",centIndex),
					    Form("Rapidity Loss %s;y_{%s}^{Reco};p_{T}^{Reco} (GeV);y_{%s}^{Reco}-y_{%s}^{Emb} (GeV)",
						 pidSymbol,pidSymbol,pidSymbol,pidSymbol),
					    nRapidityBins,rapidityMin,rapidityMax,50,0,2.0,100,-.1,.1);
  }
  
  /******** LOOP OVER THE ENTRIES OF THE CHAIN **********/
  Long64_t nEntries = fileChain->GetEntries();
  for (Long64_t iEntry=0; iEntry<nEntries; iEntry++){

    //Increment total events
    nEventsHisto->Fill(0);
    
    minimcReader->Reset();
    fileChain->GetEntry(iEntry);

    if (minimcReader->GetNEmbeddedTracks() >= minimcReader->GetNMaxEntries()){
      cout <<"The number of Embedded tracks is larger than the array length!" <<endl;
      exit (EXIT_FAILURE);
    }

    //Fill The No Cut Histograms
    embZVertexHistoNoCuts->Fill(minimcReader->GetZVertexEmb());
    embXYVertexHistoNoCuts->Fill(minimcReader->GetXVertexEmb(), minimcReader->GetYVertexEmb());

    Int_t centralityBin = GetCentralityBin(centralityTree,centralityDatabase,&runNumberLookUpTable,
					   minimcReader->GetRunNumber(),minimcReader->GetEventNumber());

    //If this is a bad event increment counter, fill histograms and skip
    if (centralityBin < 0){
      nEventsHisto->Fill(2);
      embZVertexHistoBad->Fill(minimcReader->GetZVertexEmb());
      embXYVertexHistoBad->Fill(minimcReader->GetXVertexEmb(),minimcReader->GetYVertexEmb());
      embCentralMultHistoBad->Fill(minimcReader->GetCentralMult());
      embOriginMultHistoBad->Fill(minimcReader->GetOriginalRefMult());
      badEvents->Fill(minimcReader->GetXVertexEmb(),minimcReader->GetYVertexEmb(), minimcReader->GetZVertexEmb(),
		      minimcReader->GetRunNumber(),minimcReader->GetEventNumber(),minimcReader->GetCentralMult(),minimcReader->GetOriginalRefMult());      
      continue;
    }

    //Increment the Good Event Counter
    nEventsHisto->Fill(1);

    //Fill The Good Event Level Histograms                                                                   
    nEmbTrackHisto->Fill(minimcReader->GetNEmbeddedTracks());
    nMatchTrackHisto->Fill(minimcReader->GetNMatchedTracks());
    refMultHisto->Fill(minimcReader->GetRefMult());
    originMultHisto->Fill(minimcReader->GetOriginalRefMult());
    centralityHisto->Fill(minimcReader->GetCentrality());
    centralMultHisto->Fill(minimcReader->GetCentralMult());
    embZVertexHisto->Fill(minimcReader->GetZVertexEmb());
    embEventPhiHisto->Fill(TMath::ATan2(minimcReader->GetYVertexEmb(),minimcReader->GetXVertexEmb()));
    embXYVertexHisto->Fill(minimcReader->GetXVertexEmb(),minimcReader->GetYVertexEmb());

    //****************************************************************************
    // ---- Loop Over the Embedded Tracks ----
    for (Int_t embTrackIndex=0; embTrackIndex<minimcReader->GetNEmbeddedTracks(); embTrackIndex++){
      
      //Make Sure the Track is an embedded track and that it has the right Geant PID                    
      if (minimcReader->GetEmbTrackGeantID(embTrackIndex) != GetGeantID(PIDINTEREST,CHARGE) || 
	  minimcReader->GetEmbTrackParentGeantID(embTrackIndex) != 0)
        continue;
      
      //Compute the Tracks Rapidity                                                                     
      Double_t embTrackRapidity = ComputeRapidity(minimcReader->GetEmbTrackPt(embTrackIndex),
						  minimcReader->GetEmbTrackPz(embTrackIndex),
                                                  GetParticleMass(PIDINTEREST));
      
      //Compute the Track's mTm0                                                                        
      Double_t embTrackmTm0 = ComputemTm0(minimcReader->GetEmbTrackPt(embTrackIndex),PIDINTEREST);

      
      //Fill the Embedded Track QA Histograms
      embTrackEtaPhiHisto->Fill(minimcReader->GetEmbTrackEta(embTrackIndex),
				minimcReader->GetEmbTrackPhi(embTrackIndex));
      embTrackRapidityPtHisto->Fill(embTrackRapidity,
				    minimcReader->GetEmbTrackPt(embTrackIndex));
      embTrackEtaHisto->Fill(minimcReader->GetEmbTrackEta(embTrackIndex));
      embTrackPhiHisto->Fill(minimcReader->GetEmbTrackPhi(embTrackIndex));
      embTrackRapidityHisto->Fill(embTrackRapidity);
      embTrackPtHisto->Fill(minimcReader->GetEmbTrackPt(embTrackIndex));
      embTrackmTm0Histo->Fill(embTrackmTm0);
      embTrackRapidityPtPhiHisto->Fill(embTrackRapidity,
				       minimcReader->GetEmbTrackPt(embTrackIndex),
				       minimcReader->GetEmbTrackPhi(embTrackIndex));
      embTrackRapidityPtNHitsHisto->Fill(embTrackRapidity,
					 minimcReader->GetEmbTrackPt(embTrackIndex),
					 minimcReader->GetEmbTrackNHits(embTrackIndex));
      embTrackEtaPtPhiHisto->Fill(minimcReader->GetEmbTrackEta(embTrackIndex),
				  minimcReader->GetEmbTrackPt(embTrackIndex),
				  minimcReader->GetEmbTrackPhi(embTrackIndex));

      //Make Sure the Embedded Track is in the Defined Range
      if (embTrackRapidity < rapidityMin || embTrackRapidity > rapidityMax)
	continue;
      
      //Fill the Efficiency Related Histogram
      embTrackHisto[centralityBin]->Fill(embTrackRapidity,embTrackmTm0);
      
      
    }//End Loop Over Embedded Tracks

    //*****************************************************************************
    // ---- Loop Over the Matched Tracks ----
    for (Int_t matchTrackIndex=0; matchTrackIndex<minimcReader->GetNMatchedTracks(); matchTrackIndex++){

      //Make Sure the Track is an embedded track and that it has the right Geant PID                    
      if (minimcReader->GetMatchedTrackGeantID(matchTrackIndex) != GetGeantID(PIDINTEREST,CHARGE) || 
	  minimcReader->GetMatchedTrackParentGeantID(matchTrackIndex) != 0)
        continue;
      
      if (!IsGoodTrack(minimcReader,matchTrackIndex,"Matched"))
	continue;

      //Compute the Rapidity of The Track                                                               
      Double_t matchTrackRapidityEmb = ComputeRapidity(minimcReader->GetMatchedTrackPtEmb(matchTrackIndex),
                                                       minimcReader->GetMatchedTrackPzEmb(matchTrackIndex),
                                                       GetParticleMass(PIDINTEREST));
      Double_t matchTrackRapidityReco= ComputeRapidity(minimcReader->GetMatchedTrackPtReco(matchTrackIndex),
                                                       minimcReader->GetMatchedTrackPzReco(matchTrackIndex),
                                                       GetParticleMass(PIDINTEREST));

      //Compute the mTm0 Of the Track                                                                   
      Double_t matchTrackmTm0Emb = ComputemTm0(minimcReader->GetMatchedTrackPtEmb(matchTrackIndex),PIDINTEREST);
      Double_t matchTrackmTm0Reco = ComputemTm0(minimcReader->GetMatchedTrackPtReco(matchTrackIndex),PIDINTEREST);

      //Fill the Match Track QA Histograms                                                              
      matchTrackEtaPhiHisto->Fill(minimcReader->GetMatchedTrackEtaReco(matchTrackIndex),
				  minimcReader->GetMatchedTrackPhiReco(matchTrackIndex));
      matchTrackRapidityPtHisto->Fill(matchTrackRapidityReco,
				      minimcReader->GetMatchedTrackPtReco(matchTrackIndex));
      matchTrackEtaHisto->Fill(minimcReader->GetMatchedTrackEtaReco(matchTrackIndex));
      matchTrackPhiHisto->Fill(minimcReader->GetMatchedTrackPhiReco(matchTrackIndex));
      matchTrackRapidityHisto->Fill(matchTrackRapidityReco);
      matchTrackPtHisto->Fill(minimcReader->GetMatchedTrackPtReco(matchTrackIndex));
      matchTrackmTm0Histo->Fill(matchTrackmTm0Reco);

      matchTrackRapidityPtPhiHisto->Fill(matchTrackRapidityReco,
					 minimcReader->GetMatchedTrackPtReco(matchTrackIndex),
					 minimcReader->GetMatchedTrackPhiReco(matchTrackIndex));
      matchTrackRapidityPtNHitsHisto->Fill(matchTrackRapidityReco,
					   minimcReader->GetMatchedTrackPtReco(matchTrackIndex),
					   minimcReader->GetMatchedTrackNHits(matchTrackIndex));
      matchTrackRapidityPtNHitsFitHisto->Fill(matchTrackRapidityReco,
					      minimcReader->GetMatchedTrackPtReco(matchTrackIndex),
					      minimcReader->GetMatchedTrackNHitsFit(matchTrackIndex));
      matchTrackRapidityPtNHitsPossHisto->Fill(matchTrackRapidityReco,
					       minimcReader->GetMatchedTrackPtReco(matchTrackIndex),
					       minimcReader->GetMatchedTrackNHitsPoss(matchTrackIndex));
      matchTrackRapidityPtNHitsFracHisto->Fill(matchTrackRapidityReco,
					       minimcReader->GetMatchedTrackPtReco(matchTrackIndex),
					       minimcReader->GetMatchedTrackNHitsFit(matchTrackIndex)/
					       (Double_t)minimcReader->GetMatchedTrackNHitsPoss(matchTrackIndex));
      matchTrackRapidityPtNHitsdEdxHisto->Fill(matchTrackRapidityReco,
					       minimcReader->GetMatchedTrackPtReco(matchTrackIndex),
					       minimcReader->GetMatchedTrackNHitsdEdx(matchTrackIndex));
      matchTrackRapidityPtGlobalDCAHisto->Fill(matchTrackRapidityReco,
					       minimcReader->GetMatchedTrackPtReco(matchTrackIndex),
					       minimcReader->GetMatchedTrackDCAGl(matchTrackIndex));
      matchTrackEtaPtPhiHisto->Fill(minimcReader->GetMatchedTrackEtaReco(matchTrackIndex),
				    minimcReader->GetMatchedTrackPtReco(matchTrackIndex),
				    minimcReader->GetMatchedTrackPhiReco(matchTrackIndex));
      matchTrackEtaPembPrecoHisto->Fill(minimcReader->GetMatchedTrackEtaEmb(matchTrackIndex),
					sqrt(pow(minimcReader->GetMatchedTrackPtEmb(matchTrackIndex),2)+
					     pow(minimcReader->GetMatchedTrackPzEmb(matchTrackIndex),2)),
					sqrt(pow(minimcReader->GetMatchedTrackPtReco(matchTrackIndex),2)+
					     pow(minimcReader->GetMatchedTrackPzReco(matchTrackIndex),2)));
      matchTrackEtaPtembPtrecoHisto->Fill(minimcReader->GetMatchedTrackEtaEmb(matchTrackIndex),
					  minimcReader->GetMatchedTrackPtEmb(matchTrackIndex),
					  minimcReader->GetMatchedTrackPtReco(matchTrackIndex));
      matchTrackEtaRapidityembRapidityrecoHisto->Fill(minimcReader->GetMatchedTrackEtaEmb(matchTrackIndex),
						      matchTrackRapidityEmb,
						      matchTrackRapidityReco);
      matchTrackEtaPtdEdxHisto->Fill(minimcReader->GetMatchedTrackEtaEmb(matchTrackIndex),
				     minimcReader->GetMatchedTrackPtEmb(matchTrackIndex),
				     minimcReader->GetMatchedTrackdEdx(matchTrackIndex)*pow(10,6));


      //Make Sure that the Rapidity of the Track is in the Defined Range
      if (matchTrackRapidityEmb < rapidityMin || matchTrackRapidityEmb > rapidityMax)
	continue;
      
      //Fill the Efficiency Related Histograms
      matchTrackHisto[centralityBin]->Fill(matchTrackRapidityEmb,matchTrackmTm0Emb);
      
      //Fill the Energy Loss Histograms
      pTLossHisto[centralityBin]->Fill(matchTrackRapidityReco,
				       minimcReader->GetMatchedTrackPtReco(matchTrackIndex),
				       minimcReader->GetMatchedTrackPtReco(matchTrackIndex)-
				       minimcReader->GetMatchedTrackPtEmb(matchTrackIndex));
      rapidityLossHisto[centralityBin]->Fill(matchTrackRapidityReco,
					     minimcReader->GetMatchedTrackPtReco(matchTrackIndex),
					     matchTrackRapidityReco-matchTrackRapidityEmb);
      
    }//End Loop Over Matched Tracks

    //*****************************************************************************
    // ---- Loop Over the Split Tracks ----
    for (Int_t splitTrackIndex=0; splitTrackIndex<minimcReader->GetNSplitTracks(); splitTrackIndex++){

      //Make Sure the Track is an embedded track and that it has the right Geant PID
      if (minimcReader->GetSplitTrackGeantID(splitTrackIndex) != GetGeantID(PIDINTEREST,CHARGE) ||
          minimcReader->GetSplitTrackParentGeantID(splitTrackIndex) != 0)
        continue;

      if (!IsGoodTrack(minimcReader,splitTrackIndex,"Split"))
	continue;
      
      //Fill The SplitTrack Histograms
      splitTrackEtaPtHisto->Fill(minimcReader->GetSplitTrackEtaEmb(splitTrackIndex),
				 minimcReader->GetSplitTrackPtEmb(splitTrackIndex));

    }//End Loop Over split Tracks
       
  }//End Loop Over the entries of the chain


  //*************************************************************
  //Save Everything
  outFile->cd();
  outFile->mkdir("QANoCuts");
  outFile->mkdir("UnusableEvents");
  outFile->mkdir("EventQAPlots");
  outFile->mkdir("EmbeddedTrackQAPlots");
  outFile->mkdir("MatchedTrackQAPlots");
  outFile->mkdir("SplitTrackQAPlots");
  outFile->mkdir("EfficiencyHistograms");
  outFile->mkdir("EnergyLossHistograms");

  //Save the nEvents Histogram
  nEventsHisto->Write();

  //Save the No Cuts Histograms
  outFile->cd();
  outFile->cd("QANoCuts");
  embZVertexHistoNoCuts->Write();
  embXYVertexHistoNoCuts->Write();

  //Save the Bad Event Plots
  outFile->cd();
  outFile->cd("UnusableEvents");
  embZVertexHistoBad->Write();
  embXYVertexHistoBad->Write();
  embCentralMultHistoBad->Write();
  embOriginMultHistoBad->Write();
  badEvents->Write();
  
  //Save the QA Plots
  outFile->cd();
  outFile->cd("EventQAPlots");
  nEmbTrackHisto->Write();
  nMatchTrackHisto->Write();
  refMultHisto->Write();
  originMultHisto->Write();
  centralityHisto->Write();
  centralMultHisto->Write();
  embZVertexHisto->Write();
  embEventPhiHisto->Write();
  embXYVertexHisto->Write();

  outFile->cd();
  outFile->cd("EmbeddedTrackQAPlots");
  embTrackEtaPhiHisto->Write();
  embTrackRapidityPtHisto->Write();
  embTrackEtaHisto->Write();
  embTrackPhiHisto->Write();
  embTrackRapidityHisto->Write();
  embTrackPtHisto->Write();
  embTrackmTm0Histo->Write();
  embTrackRapidityPtPhiHisto->Write();
  embTrackRapidityPtNHitsHisto->Write();
  embTrackEtaPtPhiHisto->Write();

  outFile->cd();
  outFile->cd("MatchedTrackQAPlots");
  matchTrackEtaPhiHisto->Write();
  matchTrackRapidityPtHisto->Write();
  matchTrackEtaHisto->Write();
  matchTrackPhiHisto->Write();
  matchTrackRapidityHisto->Write();
  matchTrackPtHisto->Write();
  matchTrackmTm0Histo->Write();
  matchTrackRapidityPtPhiHisto->Write();
  matchTrackRapidityPtNHitsHisto->Write();
  matchTrackRapidityPtNHitsFitHisto->Write();
  matchTrackRapidityPtNHitsPossHisto->Write();
  matchTrackRapidityPtNHitsFracHisto->Write();
  matchTrackRapidityPtNHitsdEdxHisto->Write();
  matchTrackRapidityPtGlobalDCAHisto->Write();
  matchTrackEtaPtPhiHisto->Write();
  matchTrackEtaPembPrecoHisto->Write();
  matchTrackEtaPtembPtrecoHisto->Write();
  matchTrackEtaRapidityembRapidityrecoHisto->Write();
  matchTrackEtaPtdEdxHisto->Write();

  outFile->cd();
  outFile->cd("SplitTrackQAPlots");
  splitTrackEtaPtHisto->Write();

  //Save the Efficiency Histogram
  outFile->cd();
  outFile->cd("EfficiencyHistograms");
  for (int centIndex=0; centIndex<nCentBins; centIndex++){
  embTrackHisto[centIndex]->Write();
  matchTrackHisto[centIndex]->Write();
  }

  //Save the Energy Loss Histograms
  outFile->cd();
  outFile->cd("EnergyLossHistograms");
  for (int centIndex=0; centIndex<nCentBins; centIndex++){
    pTLossHisto[centIndex]->Write();
    rapidityLossHisto[centIndex]->Write();
  }

  //Close the File
  outFile->Close();

}

/*
//___________________________________________________________
std::vector<Long64_t> BuildRunNumberLookUpTable(TTree *centralityTree, CentralityDatabase *centralityDatabase){

  std::vector<Long64_t> runNumberLookUpTable;

  //Loop Over the centrality tree and add each run number to the look up table
  for (Long64_t iEntry=0; iEntry<centralityTree->GetEntries(); iEntry++){

    centralityTree->GetEntry(iEntry);
    runNumberLookUpTable.push_back(centralityDatabase->GetRunNumber());
    
  }

  return runNumberLookUpTable;

}

//___________________________________________________________
Int_t GetCentralityBin(TTree *centralityTree, CentralityDatabase *centralityDatabase,
		       std::vector<Long64_t> *runNumberLookUpTable, Long64_t runNumber, Long64_t eventNumber){

  Int_t centralityBin(-1);

  for (unsigned int i=0; i<runNumberLookUpTable->size(); i++){

    if (runNumberLookUpTable->at(i) != runNumber)
      continue;

    centralityTree->GetEntry(i);
    centralityBin = centralityDatabase->GetCentralityBin(eventNumber);

    if (centralityBin >= 0)
      return centralityBin;
  }

  //Failure Mode
  return -1;

}


//____________________________________________________________
Bool_t IsGoodTrack(MiniMcReader *minimcReader, Int_t trackIndex, TString trackType){

  //These track cuts should be the same as what you use in your analysis

  Short_t flag(0);
  Short_t nHitsFit(0);
  Short_t nHitsPoss(0);
  Short_t nHitsdEdx(0);
  Double_t globalDCA(0);
  
  //Fill the variables depending on the track type;
  if (trackType.Contains("Matched")){
    flag      = minimcReader->GetMatchedTrackFlag(trackIndex);
    nHitsFit  = minimcReader->GetMatchedTrackNHitsFit(trackIndex);
    nHitsPoss = minimcReader->GetMatchedTrackNHitsPoss(trackIndex);
    nHitsdEdx = minimcReader->GetMatchedTrackNHitsdEdx(trackIndex);
    globalDCA = minimcReader->GetMatchedTrackDCAGl(trackIndex);
  }
  else if (trackType.Contains("Split")){
    flag      = minimcReader->GetSplitTrackFlag(trackIndex);
    nHitsFit  = minimcReader->GetSplitTrackNHitsFit(trackIndex);
    nHitsPoss = minimcReader->GetSplitTrackNHitsPoss(trackIndex);
    nHitsdEdx = minimcReader->GetSplitTrackNHitsdEdx(trackIndex);
    globalDCA = minimcReader->GetSplitTrackDCAGl(trackIndex);
  }
  else {
    cout <<"ERROR - ProcessEmbedding::IsGoodTrack() trackType is not recognized! EXITING!\n";
    exit (EXIT_FAILURE);
  }

  //Apply the cuts
  if (flag >= 1000 || flag < 0)
    return false;

  if (nHitsFit < 15)
    return false;

  if (nHitsdEdx < 10)
    return false;

  if ((double)nHitsFit/(double)nHitsPoss < 0.52)
    return false;

  if (globalDCA > 1.0)
    return false;
  
  //If it passes all the cuts it is a good track
  return true;

}

*/
