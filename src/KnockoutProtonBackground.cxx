#include <iostream>
#include <vector>
#include <fstream>
#include <string>

#include <TFile.h>
#include <TString.h>
#include <TMath.h>
#include <TSystem.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TH3F.h>
#include <TChain.h>
#include <TNtupleD.h>

#include "globalDefinitions.h"
#include "utilityFunctions.h"
#include "MiniMcReader.h"
#include "CentralityDatabaseClass.h"
#include "BinningAndCuts.h"


//MAIN________________________________________________________________________
void KnockoutProtonBackground(const char* MINIMCLIST, TString OUTPUTFILE, TString CENTDATABASEFILE,
			      Double_t ENERGY, Int_t PIDINTEREST, Int_t CHARGE){

  const int nCentBins = 9;
  TString energy = TString::Format("#sqrt{s_{NN}} = %g GeV",ENERGY);
  const char *pidSymbol = GetParticleSymbol(PIDINTEREST,CHARGE).Data();
  const char *pidName   = GetParticleName(PIDINTEREST,CHARGE).Data();

  //Open the List of miniMC Files
  ifstream miniMCList (MINIMCLIST);
  if (!miniMCList.is_open()){
    cout <<"MiniMCList not Open!" <<endl;
    exit (EXIT_FAILURE);
  }

  //Read All the Lines of the MINIMCLIST file and add them to the chain
  string miniMCFile;
  TChain *fileChain = new TChain("StMiniMcTree");
  while (miniMCList.good()){
    getline(miniMCList, miniMCFile);
    fileChain->Add(miniMCFile.c_str());
  }

  //Close the MiniMCList File
  miniMCList.close();

  //Create the MiniMC Reader
  MiniMcReader *minimcReader = new MiniMcReader((TTree *)fileChain);

  //Load the Centrality Database File
  CentralityDatabase *centralityDatabase = NULL;
  TFile *centralityFile = new TFile(CENTDATABASEFILE,"READ");
  TTree *centralityTree = NULL;
  if (centralityFile->IsZombie() == false){
    centralityTree = (TTree *)centralityFile->Get("CentralityTree");
    if (!centralityTree){
      cout <<"ERROR - ProcessEmbedding() Centrality Tree not found! EXITING!\n";
      exit (EXIT_FAILURE);
    }
    centralityTree->SetBranchAddress("Runs",&centralityDatabase);
  }

  //Build The Run Number Look Up Table
  std::vector<Long64_t> runNumberLookUpTable =
    BuildRunNumberLookUpTable(centralityTree,centralityDatabase);

  //Set Event Vertex Parameters
  centralityTree->GetEntry(0);
  const double minVz = centralityDatabase->GetMinVz();
  const double maxVz = centralityDatabase->GetMaxVz();

  //Create the output File
  TFile *outFile = new TFile(OUTPUTFILE,"RECREATE");

  //************** CREATE HISTOGRAMS **********************
  //NEvents Histogram
  TH1F *nEventsHisto = new TH1F("nEvents","Number of Events: 0=total, 1=used, 2=unused",3,0,3);
  
  //Global DCA Distribution
  std::vector< TH3F *> globalDCAHisto (nCentBins, (TH3F *)NULL);
  for (int centIndex=0; centIndex<nCentBins; centIndex++){
    globalDCAHisto.at(centIndex) = new TH3F(Form("globalDCA_%s_centIndex%02d", pidName, centIndex),
					    Form("Global DCA Distribution | %s;y_{%s}^{Reco};m_{T}-m_{%s} (GeV/c^{2});glDCA (cm)",
						 pidName,pidSymbol,pidSymbol),
					    nRapidityBins,rapidityMin,rapidityMax,nmTm0Bins,mTm0Min,mTm0Max,100,0,10);

  }//End Loop Over CentIndex
  

  //************** LOOP OVER THE ENTRIES OF THE CHAIN ********************/
  Long64_t nEntries = fileChain->GetEntries();
  for (Long64_t iEntry=0; iEntry<nEntries; iEntry++){

    //Inctrement total Events
    nEventsHisto->Fill(0);

    minimcReader->Reset();
    fileChain->GetEntry(iEntry);
    
    if (minimcReader->GetNEmbeddedTracks() >= minimcReader->GetNMaxEntries()){
      cout <<"The number of Embedded tracks is larger than the array length!" <<endl;
      exit (EXIT_FAILURE);
    }

    //Get the Centrality Bin
    Int_t centralityBin = GetCentralityBin(centralityTree,centralityDatabase,&runNumberLookUpTable,
					   minimcReader->GetRunNumber(),minimcReader->GetEventNumber());

    
    //If this is a bad event, increment the counter and skip
    if (centralityBin < 0){
      nEventsHisto->Fill(2);
      continue;
    }

    //Increment the Good Event Counter
    nEventsHisto->Fill(1);

    //******* LOOP OVER THE MATCHED TRACKS *************
    for (Int_t matchTrackIndex=0; matchTrackIndex<minimcReader->GetNMatchedTracks(); matchTrackIndex++){
      
      //Make Sure the Track is the required species
      if (minimcReader->GetMatchedTrackGeantID(matchTrackIndex) != GetGeantID(PIDINTEREST,CHARGE) )
	continue;

      //Make Sure the Track Passes all of the Track Cuts
      if (!IsGoodTrack(minimcReader,matchTrackIndex,"Matched",false))
	continue;

      //Compute the Rapidity of the Track
      Double_t matchTrackRapidityReco = ComputeRapidity(minimcReader->GetMatchedTrackPtReco(matchTrackIndex),
							minimcReader->GetMatchedTrackPzReco(matchTrackIndex),
							GetParticleMass(PIDINTEREST));

      //Compute the mT-m0 of the Track
      Double_t matchTrackmTm0Reco     = ComputemTm0(minimcReader->GetMatchedTrackPtReco(matchTrackIndex),PIDINTEREST);

      //Fill the GlobalDCA Histogram
      globalDCAHisto.at(centralityBin)->Fill(matchTrackRapidityReco,matchTrackmTm0Reco,
					     minimcReader->GetMatchedTrackDCAGl(matchTrackIndex));

    }//End Loop Over Matched Tracks


  }//End Loop Over Entries of the chain


  //************ SAVE EVERYTHING ******************
  outFile->cd();
  nEventsHisto->Write();
  for (int centIndex=0; centIndex<nCentBins; centIndex++){
    globalDCAHisto.at(centIndex)->Write();
  }

}
