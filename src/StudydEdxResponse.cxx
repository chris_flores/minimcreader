#include <iostream>
#include <vector>
#include <fstream>
#include <string>

#include <TFile.h>
#include <TString.h>
#include <TMath.h>
#include <TSystem.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TH3F.h>
#include <TChain.h>
#include <TNtupleD.h>

#include "globalDefinitions.h"
#include "utilityFunctions.h"
#include "MiniMcReader.h"
#include "CentralityDatabaseClass.h"
#include "BinningAndCuts.h"

/***************************************************
ON EVENT CUTS:
The centrality database contains the run number, event number, 
and centrality bin index for each event that has already passed the
event selection criteria for the analysis. Thus, if a centrality bin
is not found for an event it is a bad event and can be rejected. This means
we do not have to apply any event cuts in this code.

ON TRACK CUTS:
You should implement your tracks cuts in the IsGoodTrack() function which
is located in src/BinningAndCuts.cxx
*****************************************************/

//MAIN_____________________________________________
void StudydEdxResponse(const char *MINIMCLIST, TString OUTPUTFILE, TString CENTDATABASEFILE,
		       Double_t ENERGY, Int_t PIDINTEREST, Int_t CHARGE){

  TString energy = TString::Format("#sqrt{s_{NN}} = %g GeV",ENERGY);
  const char *pidSymbol = GetParticleSymbol(PIDINTEREST,CHARGE).Data();

  //Open the List of miniMC Files       
  ifstream miniMCList (MINIMCLIST);
  if (!miniMCList.is_open()){
    cout <<"MiniMCList not Open!" <<endl;
    exit (EXIT_FAILURE);
  }
  
  //Read All the Lines of the MINIMCLIST file and add them to the chain
  string miniMCFile;
  TChain *fileChain = new TChain("StMiniMcTree");
  while (miniMCList.good()){
    getline(miniMCList, miniMCFile);
    fileChain->Add(miniMCFile.c_str());
  }

  //Close the MiniMCList File  
  miniMCList.close();

  //Create the MiniMC Reader
  MiniMcReader *minimcReader = new MiniMcReader((TTree *)fileChain);
  
  //Load the Centrality Database File
  CentralityDatabase *centralityDatabase = NULL;
  TFile *centralityFile = new TFile(CENTDATABASEFILE,"READ");
  TTree *centralityTree = NULL;
  if (centralityFile->IsZombie() == false){ 
    centralityTree = (TTree *)centralityFile->Get("CentralityTree");
    if (!centralityTree){
      cout <<"ERROR - ProcessEmbedding() Centrality Tree not found! EXITING!\n";
      exit (EXIT_FAILURE);
    }
    centralityTree->SetBranchAddress("Runs",&centralityDatabase);
  }

  //Build The Run Number Look Up Table
  std::vector<Long64_t> runNumberLookUpTable =
    BuildRunNumberLookUpTable(centralityTree,centralityDatabase);

  //Set Event Vertex Parameters
  centralityTree->GetEntry(0);
  const double minVz = centralityDatabase->GetMinVz();
  const double maxVz = centralityDatabase->GetMaxVz();

  //Create the output File
  TFile *outFile = new TFile(OUTPUTFILE,"RECREATE");
  
  //************ CREATE HISTOGRAMS ***********************//
  //NEvents Histograms
  TH1F *nEventsHisto = new TH1F("nEvents","Number of Events: 0=total, 1=used, 2=unused",3,0,3);
  
  //QA Histograms for Embedded Events Before Any Cuts
  TH1F *embZVertexHistoNoCuts;
  TH2F *embXYVertexHistoNoCuts;
  embZVertexHistoNoCuts = new TH1F("embZVertexHistoNoCuts",
				   Form("V_{z} %s %s (No Cuts);z Vertex (cm)",pidSymbol,energy.Data()),
				   100,minVz-10,maxVz+10);
  embXYVertexHistoNoCuts = new TH2F("embXYVertexHistoNoCuts",
				    Form("V_{x},V_{y} %s %s (No Cuts);x Vertex (cm);y Vertex (cm)",pidSymbol,energy.Data()),
				    100,-6,6,100,-6,6);
  
  
  //QA Histograms for Embedded Event Quantities
  TH1F *embZVertexHisto;
  TH2F *embXYVertexHisto;
  
  embZVertexHisto  = new TH1F("embZVertexHisto",
			      Form("V_{z} %s %s;z Vertex (cm)",pidSymbol,energy.Data()),
			      100,minVz-10,maxVz+10);
  embXYVertexHisto = new TH2F("embXYVertexHisto",
			      Form("V_{x},V_{y} %s %s;x Vertex (cm);y Vertex (cm)",pidSymbol,energy.Data()),
			      100,-6,6,100,-6,6);
  
  //QA Histograms for the Matched Track Quantities        
  const int nParticleSpecies = 3; //Pion,Kaon,Proton
  std::vector< std::vector< TH2F *> > dEdxBetaGammaHisto(nParticleSpecies, std::vector<TH2F *> 
							 (nRapidityBins, (TH2F *)NULL));
  for (unsigned int i=0; i<nParticleSpecies; i++){
    for (unsigned int j=0; j<nRapidityBins; j++){
      dEdxBetaGammaHisto.at(i).at(j) =
	new TH2F(Form("dEdxBetaGamma_%s_yIndex%d",GetParticleName(i,CHARGE).Data(),j),
		 Form("dE/dx Vs. #beta#gamma Mass=%s | yIndex = %d",GetParticleSymbol(i,CHARGE).Data(),j),
		 250,.1,100,1000,1,250);
    }
  }
  
  /******** LOOP OVER THE ENTRIES OF THE CHAIN **********/
  Long64_t nEntries = fileChain->GetEntries();
  for (Long64_t iEntry=0; iEntry<nEntries; iEntry++){

    //Increment total events
    nEventsHisto->Fill(0);
    
    minimcReader->Reset();
    fileChain->GetEntry(iEntry);

    if (minimcReader->GetNEmbeddedTracks() >= minimcReader->GetNMaxEntries()){
      cout <<"The number of Embedded tracks is larger than the array length!" <<endl;
      exit (EXIT_FAILURE);
    }

    //Fill The No Cut Histograms
    embZVertexHistoNoCuts->Fill(minimcReader->GetZVertexEmb());
    embXYVertexHistoNoCuts->Fill(minimcReader->GetXVertexEmb(), minimcReader->GetYVertexEmb());

    Int_t centralityBin = GetCentralityBin(centralityTree,centralityDatabase,&runNumberLookUpTable,
					   minimcReader->GetRunNumber(),minimcReader->GetEventNumber());

    //If this is a bad event skip
    if (centralityBin < 0){
      nEventsHisto->Fill(2);
      continue;
    }

    //Increment the Good Event Counter
    nEventsHisto->Fill(1);

    //Fill The Good Event Level Histograms                                                                   
    embZVertexHisto->Fill(minimcReader->GetZVertexEmb());
    embXYVertexHisto->Fill(minimcReader->GetXVertexEmb(),minimcReader->GetYVertexEmb());

    //*****************************************************************************
    // ---- Loop Over the Matched Tracks ----
    for (Int_t matchTrackIndex=0; matchTrackIndex<minimcReader->GetNMatchedTracks(); matchTrackIndex++){

      //Make Sure the Track is an embedded track and that it has the right Geant PID                    
      if (minimcReader->GetMatchedTrackGeantID(matchTrackIndex) != GetGeantID(PIDINTEREST,CHARGE) || 
	  minimcReader->GetMatchedTrackParentGeantID(matchTrackIndex) != 0)
        continue;
      
      if (!IsGoodTrack(minimcReader,matchTrackIndex,"Matched"))
	continue;

      //Loop Over the particle species, compute the rapidity for each mass assumption
      //and compute the betaGamma
      for (int iSpecies=0; iSpecies<nParticleSpecies; iSpecies++){

	//Compute the Rapidity of The Track                                                               
	Double_t matchTrackRapidityReco= ComputeRapidity(minimcReader->GetMatchedTrackPtReco(matchTrackIndex),
							 minimcReader->GetMatchedTrackPzReco(matchTrackIndex),
							 GetParticleMass(iSpecies));
	
	//Make Sure that the Rapidity of the Track is in the Defined Range
	if (matchTrackRapidityReco < rapidityMin || matchTrackRapidityReco > rapidityMax)
	  continue;
	
	//Get the Rapidity Bin
	Int_t rapidityIndex = GetRapidityIndex(matchTrackRapidityReco);
	
	//Protect against bad rapidity indices
	if (rapidityIndex < 0 || rapidityIndex >= nRapidityBins){
	  continue;
	}
	
	//Compute the Beta*Gamma = pTotal/mass and Fill the Histogram
	Double_t pTotal = sqrt(pow(minimcReader->GetMatchedTrackPzReco(matchTrackIndex),2) + 
			       pow(minimcReader->GetMatchedTrackPtReco(matchTrackIndex),2));
	Double_t betaGamma = pTotal / GetParticleMass(PIDINTEREST);
	
	dEdxBetaGammaHisto.at(iSpecies).at(rapidityIndex)->
	  Fill(betaGamma,minimcReader->GetMatchedTrackdEdx(matchTrackIndex) * pow(10,6));
	
      }//End Loop Over Particle Species
      
    }//End Loop Over Matched Tracks

  }//End Loop Over the entries of the chain


  //*************************************************************
  //Save Everything
  outFile->cd();
  outFile->mkdir("QANoCuts");
  outFile->mkdir("EventQAPlots");
  outFile->mkdir("dEdxResponse");

  //Save the nEvents Histogram
  nEventsHisto->Write();

  //Save the No Cuts Histograms
  outFile->cd();
  outFile->cd("QANoCuts");
  embZVertexHistoNoCuts->Write();
  embXYVertexHistoNoCuts->Write();
  
  //Save the QA Plots
  outFile->cd();
  outFile->cd("EventQAPlots");
  embZVertexHisto->Write();
  embXYVertexHisto->Write();

  outFile->cd();
  outFile->cd("dEdxResponse");
  for (int iSpecies=0; iSpecies<nParticleSpecies; iSpecies++){
    for (unsigned int i=0; i<nRapidityBins; i++){
      if (dEdxBetaGammaHisto.at(iSpecies).at(i)->GetEntries() > 0)
	dEdxBetaGammaHisto.at(iSpecies).at(i)->Write();
    }
  }

  //Close the File
  outFile->Close();

}
