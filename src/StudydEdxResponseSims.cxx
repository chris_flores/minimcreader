#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <utility>

#include <TFile.h>
#include <TString.h>
#include <TMath.h>
#include <TSystem.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TH3F.h>
#include <TChain.h>
#include <TNtupleD.h>

#include "globalDefinitions.h"
#include "utilityFunctions.h"
#include "MiniMcReader.h"
#include "StRefMultExtendedCorr.h"
#include "CentralityDatabaseClass.h"
#include "BinningAndCuts.h"

//MAIN_______________________________________________________________________
void StudydEdxResponseSims(const char *MINIMCLIST, TString OUTPUTFILE, Double_t ENERGY){

  const int nCentBins(9);
  
  TString energySym = TString::Format("AuAu%02d",(int)ENERGY);
  TString energy = TString::Format("#sqrt{s_{NN}} = %g GeV",ENERGY);
  
  //Open the List of miniMC Files
  ifstream miniMCList (MINIMCLIST);
  if (!miniMCList.is_open()){
    cout <<"MiniMCList not Open!" <<endl;
    exit (EXIT_FAILURE);
  }
  
  //Read All the Lines of the MINIMCLIST file and add them to the chain
  string miniMCFile;
  TChain *fileChain = new TChain("StMiniMcTree");
  while (miniMCList.good()){
    getline(miniMCList, miniMCFile);
    fileChain->Add(miniMCFile.c_str());
  }

  //Close the MiniMCList File
  miniMCList.close();

  //Create the MiniMC Reader
  MiniMcReader *minimcReader = new MiniMcReader((TTree *)fileChain);
  cout <<"Created the MiniMCReader! " <<endl;
  
  //Create the output File
  TFile *outFile = new TFile(OUTPUTFILE,"RECREATE");

  //Create and Initialize the StRefMultExtendedCorr Object
  StRefMultExtendedCorr stRefMultExtendedCorr;
  stRefMultExtendedCorr.Initialize(ENERGY);

  //********** PARTICLES **************************
  const int nParticles(9); 

  //********** CREATE HISTOGRAMS ******************
  // ENERGY LOSS
  std::vector < std::vector < TH3D *> > dEdxHisto
    (nParticles, std::vector < TH3D *>
     (nParticles, (TH3D *) NULL));
  std::vector < std::vector < TH3D *> > zTPCHisto
    (nParticles, std::vector < TH3D *>
     (nParticles, (TH3D *) NULL));

  for (int iInterest=0; iInterest<nParticles; iInterest++){
    for (int iConfound=0; iConfound<nParticles; iConfound++){

      dEdxHisto.at(iInterest).at(iConfound) =
	new TH3D(Form("dEdxHisto_%s_%s",
		      GetParticleName(iInterest).Data(),
		      GetParticleName(iConfound).Data()),
		 Form("Energy Loss in the TPC | PID = %s | Confound = %s;y_{%s};m_{T}-m_{%s} (GeV/c^{2}); dE/dx (KeV/cm)",
		      GetParticleName(iInterest).Data(),
		      GetParticleName(iConfound).Data(),
		      GetParticleSymbol(iInterest).Data(),
		      GetParticleSymbol(iInterest).Data()),
		 nRapidityBins,rapidityMin,rapidityMax,
		 nmTm0Bins,mTm0Min,mTm0Max,
		 1000,0,50);

      zTPCHisto.at(iInterest).at(iConfound) =
	new TH3D(Form("zTPCHisto_%s_%s",
		      GetParticleName(iInterest).Data(),
		      GetParticleName(iConfound).Data()),
		 Form("Z_{TPC} | PID = %s | Confound = %s;y_{%s};m_{T}-m_{%s} (GeV/c^{2}); Z_{TPC}",
		      GetParticleName(iInterest).Data(),
		      GetParticleName(iConfound).Data(),
		      GetParticleSymbol(iInterest).Data(),
		      GetParticleSymbol(iInterest).Data()),
		 nRapidityBins,rapidityMin,rapidityMax,
		 nmTm0Bins,mTm0Min,mTm0Max,
		 600,-2,4);
      
    }//End Loop Over iConfound
  }//End Loop Over iInterest

  //FILL THE DEDX HISTOGRAMS
  //*********** LOOP OVER THE ENTRIES OF THE CHAIN ********************
  Long64_t nEntries = fileChain->GetEntries();
  cout <<"Number of Entries in the Chain: " <<nEntries <<endl;
  for (Long64_t iEntry=0; iEntry<nEntries; iEntry++){
    
    //Reset the Minimc Reader
    minimcReader->Reset();

    //Get the Entry in the Chain
    fileChain->GetEntry(iEntry);
    
    //Check the length of the internal arrays
    if (minimcReader->GetNEmbeddedTracks() >= minimcReader->GetNMaxEntries()){
      cout <<"NEmbTracks: " <<minimcReader->GetNEmbeddedTracks() <<" MaxEntries: "
	   <<minimcReader->GetNMaxEntries() <<endl;
      cout <<"The number of Embedded tracks is larger than the array length!" <<endl;
      exit (EXIT_FAILURE);
    }

    //Define the allowed eta range using the zVertex
    Double_t etaMin(-10), etaMax(10);
    Double_t zVertex = minimcReader->GetZVertexEmb();
    if (fabs(zVertex) <= 100.){
      etaMin = -0.5;
      etaMax = 0.5;
    }
    else if (zVertex > 100. && zVertex <=200.){
      etaMin = -1.0;
      etaMax = 0.0;
    }
    else if (zVertex < -100. && zVertex >= -200.){
      etaMin = 0.0;
      etaMax = 1.0;
    }

    //****** DETERMINE THE CENTRALITY BIN *******
    //Compute the Number of Good Matched Tracks
    int goodMatchedTracks(0);
    for (Int_t matchTrackIndex=0; matchTrackIndex<minimcReader->GetNMatchedTracks(); matchTrackIndex++){
      
      //Apply the Eta Cut
      if (minimcReader->GetMatchedTrackEtaReco(matchTrackIndex) > etaMax ||
          minimcReader->GetMatchedTrackEtaReco(matchTrackIndex) < etaMin)
        continue;
      
      //Increment the number of good matched tracks
      if (IsGoodTrack(minimcReader,matchTrackIndex,"Matched"))
        goodMatchedTracks++;
      
    }//End Loop Over the matched Tracks
    
    //The StRefMultExtendedCorr object needs a trigger ID to do the correction
    //In real data the correction depends on the trigger ID, but in simulated data
    //the trigger does not matter. So to satisfy the requirement for a trigger ID
    //we simply choose the first trigger ID for each energy.
    
    long triggerID(0);
    if (fabs(ENERGY-7.7) < .2)
      triggerID = 290001;
    else if (fabs(ENERGY-11.5) < .2)
      triggerID = 310014;
    else if (fabs(ENERGY-14.5) < .2)
      triggerID = 440015;
    else if (fabs(ENERGY-19.6) < .2)
      triggerID = 340001;
    else if (fabs(ENERGY-27.0) < .2)
      triggerID = 360001;
    else if (fabs(ENERGY-39.0) < .2)
      triggerID = 280001;
    else if (fabs(ENERGY-62.4) < .2)
      triggerID = 270001;
    
    //Correct the number of good matched tracks in the eta refion for
    //how the detector acceptance changes with respect to the zVertex location.
    Double_t correctedGoodTracks = stRefMultExtendedCorr.GetCorrectedRefMult(triggerID,zVertex,goodMatchedTracks);
    
    //Conver the number of correctedGoodTracks to RefMult
    Int_t refMult = TMath::Nint(stRefMultExtendedCorr.ConvertToRefMult(triggerID,zVertex,correctedGoodTracks));
    
    //Get the Centrality Bin of the Event (skip if bad centrality bin)
    int centBin = GetCentralityBinOfSimulatedEvent(refMult,ENERGY);
    if (centBin < 0)
      continue;

    //****** LOOP OVER THE MATCHED TRACKS AND FILL HISTOGRAMS ******
    for (Int_t matchTrackIndex=0; matchTrackIndex<minimcReader->GetNMatchedTracks(); matchTrackIndex++){

      //Make Sure the Track passes our cuts
      if (!IsGoodTrack(minimcReader,matchTrackIndex,"Matched"))
	continue;
      
      //Get the Track's PID Index
      int pidTrue =
	ConvertGeantIDtoParticleIndex(minimcReader->GetMatchedTrackGeantID(matchTrackIndex)).first;
      if (pidTrue < 0 || pidTrue > nParticles)
	continue;
      
      //Loop Over all of the Particles of Interest
      for (int iInterest=0; iInterest<nParticles; iInterest++){

	//Compute the Kinematics Assuming the Particle of Interest's mass
	double mass = GetParticleMass(iInterest);
	Double_t rapidity = ComputeRapidity(minimcReader->GetMatchedTrackPtReco(matchTrackIndex),
					    minimcReader->GetMatchedTrackPzReco(matchTrackIndex),
					    GetParticleMass(iInterest));
	Double_t mTm0 = ComputemTm0(minimcReader->GetMatchedTrackPtReco(matchTrackIndex),iInterest);

	//Fill the Energy Loss Histogram corresponding to the Track's true PID
	dEdxHisto.at(iInterest).at(pidTrue)->
	  Fill(rapidity,mTm0,
	       minimcReader->GetMatchedTrackdEdx(matchTrackIndex) * pow(10,6) );

      }//End Loop Over iInterest
      
    }//End Loop Over the Matched Tracks
    
  }//End Loop Over Entries of Chain


  //USE THE DEDX HISTOGRAMS TO MAKE THE ZTPC HISTOGRAMS
  //*********** LOOP OVER THE ENTRIES OF THE CHAIN ********************
  for (Long64_t iEntry=0; iEntry<nEntries; iEntry++){
    
    //Reset the Minimc Reader
    minimcReader->Reset();

    //Get the Entry in the Chain
    fileChain->GetEntry(iEntry);
    
    //Check the length of the internal arrays
    if (minimcReader->GetNEmbeddedTracks() >= minimcReader->GetNMaxEntries()){
      cout <<"NEmbTracks: " <<minimcReader->GetNEmbeddedTracks() <<" MaxEntries: "
	   <<minimcReader->GetNMaxEntries() <<endl;
      cout <<"The number of Embedded tracks is larger than the array length!" <<endl;
      exit (EXIT_FAILURE);
    }

    //Define the allowed eta range using the zVertex
    Double_t etaMin(-10), etaMax(10);
    Double_t zVertex = minimcReader->GetZVertexEmb();
    if (fabs(zVertex) <= 100.){
      etaMin = -0.5;
      etaMax = 0.5;
    }
    else if (zVertex > 100. && zVertex <=200.){
      etaMin = -1.0;
      etaMax = 0.0;
    }
    else if (zVertex < -100. && zVertex >= -200.){
      etaMin = 0.0;
      etaMax = 1.0;
    }

    //****** DETERMINE THE CENTRALITY BIN *******
    //Compute the Number of Good Matched Tracks
    int goodMatchedTracks(0);
    for (Int_t matchTrackIndex=0; matchTrackIndex<minimcReader->GetNMatchedTracks(); matchTrackIndex++){
      
      //Apply the Eta Cut
      if (minimcReader->GetMatchedTrackEtaReco(matchTrackIndex) > etaMax ||
          minimcReader->GetMatchedTrackEtaReco(matchTrackIndex) < etaMin)
        continue;
      
      //Increment the number of good matched tracks
      if (IsGoodTrack(minimcReader,matchTrackIndex,"Matched"))
        goodMatchedTracks++;
      
    }//End Loop Over the matched Tracks
    
    //The StRefMultExtendedCorr object needs a trigger ID to do the correction
    //In real data the correction depends on the trigger ID, but in simulated data
    //the trigger does not matter. So to satisfy the requirement for a trigger ID
    //we simply choose the first trigger ID for each energy.
    
    long triggerID(0);
    if (fabs(ENERGY-7.7) < .2)
      triggerID = 290001;
    else if (fabs(ENERGY-11.5) < .2)
      triggerID = 310014;
    else if (fabs(ENERGY-14.5) < .2)
      triggerID = 440015;
    else if (fabs(ENERGY-19.6) < .2)
      triggerID = 340001;
    else if (fabs(ENERGY-27.0) < .2)
      triggerID = 360001;
    else if (fabs(ENERGY-39.0) < .2)
      triggerID = 280001;
    else if (fabs(ENERGY-62.4) < .2)
      triggerID = 270001;
    
    //Correct the number of good matched tracks in the eta refion for
    //how the detector acceptance changes with respect to the zVertex location.
    Double_t correctedGoodTracks = stRefMultExtendedCorr.GetCorrectedRefMult(triggerID,zVertex,goodMatchedTracks);
    
    //Conver the number of correctedGoodTracks to RefMult
    Int_t refMult = TMath::Nint(stRefMultExtendedCorr.ConvertToRefMult(triggerID,zVertex,correctedGoodTracks));
    
    //Get the Centrality Bin of the Event (skip if bad centrality bin)
    int centBin = GetCentralityBinOfSimulatedEvent(refMult,ENERGY);
    if (centBin < 0)
      continue;

    //****** LOOP OVER THE MATCHED TRACKS AND FILL HISTOGRAMS ******
    for (Int_t matchTrackIndex=0; matchTrackIndex<minimcReader->GetNMatchedTracks(); matchTrackIndex++){

      //Make Sure the Track passes our cuts
      if (!IsGoodTrack(minimcReader,matchTrackIndex,"Matched"))
	continue;
      
      //Get the Track's PID Index
      int pidTrue =
	ConvertGeantIDtoParticleIndex(minimcReader->GetMatchedTrackGeantID(matchTrackIndex)).first;
      if (pidTrue < 0 || pidTrue > nParticles)
	continue;
      
      //Loop Over all of the Particles of Interest
      for (int iInterest=0; iInterest<nParticles; iInterest++){

	//Compute the Kinematics Assuming the Particle of Interest's mass
	double mass = GetParticleMass(iInterest);
	Double_t rapidity = ComputeRapidity(minimcReader->GetMatchedTrackPtReco(matchTrackIndex),
					    minimcReader->GetMatchedTrackPzReco(matchTrackIndex),
					    GetParticleMass(iInterest));
	Double_t mTm0 = ComputemTm0(minimcReader->GetMatchedTrackPtReco(matchTrackIndex),iInterest);

	//Find the Bins in the dEdx Histogram corresponding to this particles kinematics
	int yBin = dEdxHisto.at(iInterest).at(iInterest)->GetXaxis()->FindBin(rapidity);
	int mTm0Bin = dEdxHisto.at(iInterest).at(iInterest)->GetYaxis()->FindBin(mTm0);

	//Create a Teporary Projection Histogram
	TH1D *hTemp = dEdxHisto.at(iInterest).at(iInterest)->ProjectionZ("pz",yBin,yBin,mTm0Bin,mTm0Bin);
	double dEdxExpected = hTemp->GetMean();

	double dEdxMeasured = minimcReader->GetMatchedTrackdEdx(matchTrackIndex) * pow(10,6);

	//Compute the ZTPC
	double zTPC = TMath::Log(dEdxMeasured / dEdxExpected);
	
	//Fill the Energy Loss Histogram corresponding to the Track's true PID
	zTPCHisto.at(iInterest).at(pidTrue)->
	  Fill(rapidity,mTm0,zTPC);

	if (hTemp)
	  delete hTemp;
	
      }//End Loop Over iInterest
      
    }//End Loop Over the Matched Tracks
    
  }//End Loop Over Entries of Chain

  
  //Save Everything
  outFile->cd();
  for (int iInterest=0; iInterest<nParticles; iInterest++){
    for (int iConfound=0; iConfound<nParticles; iConfound++){

      dEdxHisto.at(iInterest).at(iConfound)->Write();
      zTPCHisto.at(iInterest).at(iConfound)->Write();
      
    }//End Loop Over iConfound
  }//End Loop Over iInterest

}
