//This code constructs the multiplicity distributions of matched tracks
//from the minimc files. It is most useful as the first step in determining
//the centrality of simulated events.

#include <iostream>
#include <vector>
#include <fstream>
#include <string>

#include <TFile.h>
#include <TString.h>
#include <TMath.h>
#include <TSystem.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TH3F.h>
#include <TChain.h>
#include <TNtupleD.h>

#include "globalDefinitions.h"
#include "utilityFunctions.h"
#include "MiniMcReader.h"
#include "StRefMultExtendedCorr.h"
#include "CentralityDatabaseClass.h"
#include "BinningAndCuts.h"

//MAIN_____________________________________________________________________
void ConstructMultiplicityDistributions(const char *MINIMCLIST, TString OUTPUTFILE,Double_t ENERGY){

  TString energySym = TString::Format("AuAu%02d",(int)ENERGY);
  TString energy = TString::Format("#sqrt{s_{NN}} = %g GeV",ENERGY);

  //Open the List of miniMC Files
  ifstream miniMCList (MINIMCLIST);
  if (!miniMCList.is_open()){
    cout <<"MiniMCList not Open!" <<endl;
    exit (EXIT_FAILURE);
  }

  //Read All the Lines of the MINIMCLIST file and add them to the chain
  string miniMCFile;
  TChain *fileChain = new TChain("StMiniMcTree");
  while (miniMCList.good()){
    getline(miniMCList, miniMCFile);
    fileChain->Add(miniMCFile.c_str());
  }

  //Close the MiniMCList File 
  miniMCList.close();

  //Create the MiniMC Reader
  MiniMcReader *minimcReader = new MiniMcReader((TTree *)fileChain);

  //Create the output File
  TFile *outFile = new TFile(OUTPUTFILE,"RECREATE");

  //Create and Initialize the StRefMultExtendedCorr Object
  StRefMultExtendedCorr stRefMultExtendedCorr;
  stRefMultExtendedCorr.Initialize(ENERGY);


  //********************** CREATE HISTOGRAMS ***************************
  //Multiplicity Histograms
  TH1F *matchedTrackMult = 
    new TH1F(Form("matchedTrackMult_%s",energySym.Data()),
	     Form("Matched Track Multiplicity %s;N^{Match}",energy.Data()),5000,0,5000);
  TH1F *goodMatchedTrackMult =
    new TH1F(Form("goodMatchedTrackMult_%s",energySym.Data()),
	     Form("Good Matched Track Multiplicity %s ;N^{GoodMatch} (|#eta|<0.5)",energy.Data()),5000,0,5000);
  TH1F *correctedRefMult =
    new TH1F(Form("correctedRefMult_%s",energySym.Data()),
	     Form("Corrected RefMult %s; Corrected RefMult",energy.Data()),5000,0,5000);


  //*********** LOOP OVER THE ENTRIES OF THE CHAIN ********************
  Long64_t nEntries = fileChain->GetEntries();
  for (Long64_t iEntry=0; iEntry<nEntries; iEntry++){

    //Reset the Minimc Reader
    minimcReader->Reset();

    //Get the Entry in the Chain
    fileChain->GetEntry(iEntry);

    //Define the allowed eta range using the zVertex
    Double_t zVertex = minimcReader->GetZVertexEmb();
    
    Double_t etaMin(-10), etaMax(10);
    //ColliderCenter
    if (fabs(zVertex) <= 100.){
      etaMin = -0.5;
      etaMax = 0.5;
    }
    //ColliderNegY
    else if (zVertex > 100. && zVertex <=200.){
      etaMin = -1.0;
      etaMax = 0.0;
      goodMatchedTrackMult->GetXaxis()->SetTitle(Form("N^{GoodMatch} (%g < #eta < %g)",etaMin,etaMax));
    }
    //ColliderPosY
    else if (zVertex < -100. && zVertex >= -200.){
      etaMin = 0.0;
      etaMax = 1.0;
      goodMatchedTrackMult->GetXaxis()->SetTitle(Form("N^{GoodMatch} (%g < #eta < %g)",etaMin,etaMax));
    }
    
    //*****************************************************************************
    // ---- Loop Over the Matched Tracks ---- 
    int matchedTracks(0), goodMatchedTracks(0);
    for (Int_t matchTrackIndex=0; matchTrackIndex<minimcReader->GetNMatchedTracks(); matchTrackIndex++){
      
      //Apply the Eta Cut
      if (minimcReader->GetMatchedTrackEtaReco(matchTrackIndex) > etaMax ||
	  minimcReader->GetMatchedTrackEtaReco(matchTrackIndex) < etaMin)
	continue;
      
      //Increment the number of total matched tracks
      matchedTracks++;
      
      //Check if this is a good track, skip if not
      if (!IsGoodTrack(minimcReader,matchTrackIndex,"Matched"))
	continue;

      //Increment the number of good Matched Tracks
      goodMatchedTracks++;

    }//End Loop Over the matched Tracks

    //The StRefMultExtendedCorr object needs a trigger ID to do the correction
    //In real data the correction depends on the trigger ID, but in simulated data
    //the trigger does not matter. So to satisfy the requirement for a trigger ID
    //we simply choose the first trigger ID for each energy.
    long triggerID(0);
    if (fabs(ENERGY-7.7) < .2)
      triggerID = 290001;
    else if (fabs(ENERGY-11.5) < .2)
      triggerID = 310014;
    else if (fabs(ENERGY-14.5) < .2)
      triggerID = 440015;
    else if (fabs(ENERGY-19.6) < .2)
      triggerID = 340001;
    else if (fabs(ENERGY-27.0) < .2)
      triggerID = 360001;
    else if (fabs(ENERGY-39.0) < .2)
      triggerID = 280001;
    else if (fabs(ENERGY-62.4) < .2)
      triggerID = 270001;    

    //Correct the number of good matched tracks in the eta region for
    //the how the detector acceptance changes with respect to the events z-vertex
    double correctedGoodTracks =
      stRefMultExtendedCorr.GetCorrectedRefMult(triggerID,zVertex,goodMatchedTracks);
    
    //Convert the acceptance corrected good track number into refmult
    int refMult =
      TMath::Nint(stRefMultExtendedCorr.ConvertToRefMult(triggerID,zVertex,correctedGoodTracks));
    
    //Fill the multiplicity histograms
    matchedTrackMult->Fill(matchedTracks);
    goodMatchedTrackMult->Fill(goodMatchedTracks);
    correctedRefMult->Fill(refMult);


  }//End Loop Over the Entries in the Chain

  outFile->cd();
  matchedTrackMult->Write();
  goodMatchedTrackMult->Write();
  correctedRefMult->Write();
  
}
