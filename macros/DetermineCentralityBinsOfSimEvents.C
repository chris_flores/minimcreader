//This macro simiply reports the centrality bin cuts for 9 centrality bins
//using the multiplicity distribution obtained from src/Background/ConstructMultiplicityDistributions.

//Note that it assumes the entire cross section of the collision system is 
//measured and is therefore only suitable for simulated events.

//Note the cuts must be implemented by hand in the BinningAndCuts.cxx source file

#include <iostream>
#include <vector> 
#include <utility>

//___DEFINED BELOW_____________________________________________________________________
std::vector< double > DetermineCentralityCuts(TH1F *histo, std::vector<double> *centPercents);


//___MAIN______________________________________________________________________________
void DetermineCentralityBinsOfSimEvents(const char *MULTIPLICITYFILE, Double_t ENERGY){

  //Centrality Bin Percentiles
  std::vector <double> centralityPercents(9);
  centralityPercents.at(8) = 0.05;
  centralityPercents.at(7) = 0.1;
  centralityPercents.at(6) = 0.2;
  centralityPercents.at(5) = 0.3;
  centralityPercents.at(4) = 0.4;
  centralityPercents.at(3) = 0.5;
  centralityPercents.at(2) = 0.6;
  centralityPercents.at(1) = 0.7;
  centralityPercents.at(0) = 0.8;


  TString energySym = TString::Format("AuAu%02d",(int)ENERGY);
  TString energy = TString::Format("#sqrt{s_{NN}} = %g GeV",ENERGY);

  //Open the Multiplicity File and get the histogram
  TFile *file = new TFile(MULTIPLICITYFILE,"READ");
  TH1F *multHisto = (TH1F *)file->Get(Form("goodMatchedTrackMult_%s",energySym.Data()));
  

  //Find the last bin and set the axis range
  int lastBin = multHisto->FindLastBinAbove();
  multHisto->GetXaxis()->SetRangeUser(0,multHisto->GetBinCenter(lastBin)*1.1);

  TCanvas *canvas = new TCanvas("multCanvas","multCanvas",20,20,800,600);
  canvas->SetLogy();

  multHisto->Draw();

  std::vector< double > centralityDefinitions = DetermineCentralityCuts(multHisto,&centralityPercents);
  for (unsigned int iCentBin=0; iCentBin<centralityDefinitions.size(); iCentBin++){
    cout <<centralityPercents.at(iCentBin) <<"\t" <<centralityDefinitions.at(iCentBin) <<endl;
  }

}

//______________________________________________________________________
std::vector< double > DetermineCentralityCuts(TH1F *histo, std::vector<double> *centPercents){

  //Return a vector of the multiplicity values defining the
  //9 entrality bins as defined below

  std::vector< double > centCuts(9,0);

  //Get the Entries of the Histogram
  Double_t nEntries = histo->GetEntries();

  //Integrate until the cuts are found for each centrality bin
  for (int iCentBin=centPercents->size()-1; iCentBin>=0; iCentBin--){
    Double_t partialIntegral(0);
    Double_t percentIntegral(0);
    int currentBin = histo->FindLastBinAbove();
    while (percentIntegral < centPercents->at(iCentBin)){
      partialIntegral += histo->GetBinContent(currentBin);

      percentIntegral = partialIntegral / nEntries;

      currentBin--;
    }
    
    centCuts.at(iCentBin) = histo->GetBinLowEdge(currentBin);

  }//End Loop Over Centrality Bins

  return centCuts;


}

