void RunProcessEmbedding(TString MINIMCLIST, TString OUTPUTFILE, TString CENTDATABASEFILE,
			 Double_t ENERGY, Int_t PIDINTEREST, Int_t CHARGE){

  gSystem->Load("../bin/utilityFunctions_cxx.so");
  gSystem->Load("../bin/MiniMcReader_cxx.so");
  gSystem->Load("../bin/CentralityDatabaseClass_cxx.so");
  gSystem->Load("../bin/BinningAndCuts_cxx.so");
  gSystem->Load("../bin/ProcessEmbedding_cxx.so");

  ProcessEmbedding(MINIMCLIST.Data(),OUTPUTFILE,CENTDATABASEFILE,ENERGY,PIDINTEREST,CHARGE);
 

  
}
